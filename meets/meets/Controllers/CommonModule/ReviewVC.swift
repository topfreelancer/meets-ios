//
//  ReviewVC.swift
//  meets
//
//  Created by top Dev on 08.12.2020.
//

import UIKit
import Kingfisher
import Cosmos

class ReviewVC: BaseVC, UITextViewDelegate{

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var cus_review: CosmosView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addleftButton()
        self.title = "レビューを投稿する"
        textView.text = "レビューを書いてください"
        textView.textColor = UIColor.darkGray
        textView.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 0)
        textView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    func addleftButton() {
        
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "back")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(backBtnclicked), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 35).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 35).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func backBtnclicked() {
        if chattingOptionVC == .fromHome{
            self.gotoTabControllerWithIndex(0)
        }else if chattingOptionVC == .fromPeopleVC{
            self.gotoTabControllerWithIndex(1)
        }
    }
    
    @IBAction func reviewBtnClicked(_ sender: Any) {
        self.showLoadingView(vc: self)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.hideLoadingView()
            // goto home
            if chattingOptionVC == .fromHome{
                self.gotoTabControllerWithIndex(0)
            }else if chattingOptionVC == .fromPeopleVC{
                self.gotoTabControllerWithIndex(1)
            }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.darkGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "レビューを書いてください"
            textView.textColor = UIColor.darkGray
        }
    }
}
