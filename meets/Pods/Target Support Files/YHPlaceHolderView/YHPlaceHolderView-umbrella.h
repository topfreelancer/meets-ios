#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "DefaultView.h"
#import "NSObject+MethodSwizzle.h"
#import "UICollectionView+PlaceHolderView.h"
#import "UITableView+PlaceHolderView.h"

FOUNDATION_EXPORT double YHPlaceHolderViewVersionNumber;
FOUNDATION_EXPORT const unsigned char YHPlaceHolderViewVersionString[];

