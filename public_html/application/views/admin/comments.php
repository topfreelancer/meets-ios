<?php $this->load->view('admin/header'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
		<div class='del_res'>
		<?php
		if(isset($succes_message))
{
	
	echo '<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                  '.$succes_message.'
                  </div>';
}	
if(isset($error_message))
{
	echo '<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                  '.$error_message.'
                  </div>';
}

?>
</div>
        <section class="content-header">
          <h1>
            <?php
              echo "コメント";
            ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php  echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> ホーム</a></li>
            <li class="active">コメント</li>
          </ol>
        </section>

     
		    <!-- Main content -->
        <section class="content">
          <div class="row">
           
		      <div class="row">
            <div class="col-md-12">
				      <div class="box">
                <div class="box-header">
                  <!-- <h3 class="box-title">List of Booking</h3> -->
                  <!-- Export Data -->
                  <!-- <input type="submit" class="btn btn-danger delete-row" name="" style="float:right" value="Delete"/> -->
                  <!-- <a class="btn btn-success" href='<?= base_url() ?>index.php/admin/<?= $type; ?>_exportCSV' style="float:right; margin-right:10px;">Export to CSV</a> -->
                </div><!-- /.box-header -->
    
                <div class="box-body table-responsive">
                  <table id="example1" class="table table-bordered table-striped" >
                    
                    <thead>
                      <tr>
                        <th>号</th>
                        <th>投稿 ID</th>
                        <th>ユーザー名</th>
                        <th>ユーザー写真</th>
                        <th>コンテンツ</th>
                        <th>日付</th>
                      </tr>
                    </thead>
                  
                    <tbody>
              
              <?php
                $i = 1;
                foreach($fetch as $value) {                  
              ?>
                    <tr id='<?php echo $value->id; ?>'>
                      <td><?php echo $i ?></td>
                      <td><?php echo $value->post_id; ?></td>
                      <td >
                        <a href="<?php  echo base_url();?>admin/user-detail/<?= $value->user_id; ?>">
                              <?php echo $value->user_name; ?>
                        </a>
                      </td>
                      <td class="col-md-2 col-lg-2 col-xs-2" ><img width="90%" style="max-height: 150px" src=<?php echo $value->user_photo;?> />
                      </td>
                      <td class="col-md-6"><?php echo $value->content; ?></td>
                      <td><?php echo $value->created_at; ?></td>
                    </tr>
              <?php $i++; }?>                    
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
		
      </div><!-- /.content-wrapper -->
	 
  <?php $this->load->view('admin/footer'); ?>
  
 