<?php $this->load->view('admin/header'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
		<div class='del_res'>
		<?php
		if(isset($succes_message))
{
	
	echo '<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                  '.$succes_message.'
                  </div>';
}	
if(isset($error_message))
{
	echo '<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                  '.$error_message.'
                  </div>';
}

?>
</div>
        <section class="content-header">
          <h1>
            ユーザーの詳細
          </h1>
		      <ol class="breadcrumb">
            <li><a href="<?php  echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> ホーム</a></li>
            <li class="active">ユーザー</li>
            <li class="active">ユーザーの詳細</li>
          </ol>
        </section>

     
		   <!-- Main content -->
        <section class="content">
          
           
		      <div class="row">
            <div class="col-md-12">
				      <div class="box-header">
                <div class="row">
                <h4 style="margin-top: 20px;"><strong>ユーザーの詳細</strong></h4>
                  <div class="col-md-4 col-lg-2 col-xs-5">
                    <img width="auto" height="200px" style="margin-top: 10px;" src=<?php echo $user[0]->picture;?> width/>
                  </div>
                  <div class="col-md-8 col-lg-10 col-xs-7">
                      <h4>ユーザー名: <span style="color: #1ab300;font-weight:700"><?php echo $user[0]->username; ?></span></h4><!-- user  name -->
                      <h4>場所: <span style="color: #1ab300;font-weight:700"><?php echo $user[0]->user_location; ?></span></h4><!-- location -->
                      <h4 style="margin-bottom:0">いいねの数: <span style="color: #1ab300;font-weight:700"><?php echo $user[0]->follower; ?></span></h4><!-- number of follower -->
                      <h4 style="margin-bottom:0">誕生日: <span style="color: #1ab300;font-weight:700"><?php echo $user[0]->user_birthday; ?></span></h4><!-- birthday -->
                  </div>

                <h4 style="margin-left:10px;margin-top:100px;padding-top: 100px"><strong>ユーザーのメッセー</strong></h4>
                <div class="box-body table-responsive">
                  <table id="example1" class="table table-bordered table-striped" >
                     <thead>
                        <tr>
                            <th>号</th>
                            <th>ユーザーのメッセー ID</th>
                            <th>内容</th>
                            <th>日付</th>
                        </tr>
                    </thead>
                   
                    <tbody>
                  <?php $i = 1; foreach($posts as $value) { ?>
                      <tr id='<?php echo $value->id; ?>'>
                          <td><?php echo $i; ?></td>
                          <td><?php echo $value->id; ?></td>

                          <td class="col-md-6" ><?php echo $value->post_content; ?>
                          </td>
                          </td>
                          <td ><?php echo $value->created_at; ?></td>
                      </tr>
                  <?php $i++; }?>
                    </tbody>
                  </table>
                </div>
                </div>
		
                </div><!-- /.box-body -->
              </div><!-- /.box -->
			  
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
		
		
		
      </div><!-- /.content-wrapper -->
	 

  <?php $this->load->view('admin/footer'); ?>
  
 