<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> MeetsApp </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/plugins/iCheck/flat/blue.css">
	 <link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/plugins/iCheck/all.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	<link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/dist/css/style.css">
	<link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/dist/css/popModal.css">
	<!-- Select2 -->
	<link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/plugins/select2/select2.min.css">
	<link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/dist/css/AdminLTE.css">
	<!-- DataTables -->
	<link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/plugins/datatables/dataTables.bootstrap.css">
    <link rel="icon" href="/assets/admin/images/fav.png" sizes="32x32">
	<script type="text/javascript">
		var baseurl = "<?php echo base_url(); ?>admin/";
	</script>
<!-- <script src='https://kit.fontawesome.com/a076d05399.js'></script> -->
	<script src="<?php  echo base_url();?>assets/admin/dist/js/jquery-1.12.0.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="<?php  echo base_url();?>assets/admin/https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="<?php  echo base_url();?>assets/admin/https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

	<header class="main-header">
        <!-- Logo -->
		<a href='<?php  echo base_url();?>admin/dashboard' class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b></b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>MeetsApp</b></span>
        </a>
        <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"><span class="sr-only">Toggle navigation</span></a>
        <div class="navbar-custom-menu" style="font-size:13pt !important">
            <ul class="nav navbar-nav">
                <li><a href="#" class="toggle-requestfullscreen"><i class="fa fa-arrows-alt"></i></a></li>
                <li><a href="#" class="toggle-exitfullscreen" style='display: none'><i class="fa fa-arrows-alt"></i></a></li>
            </ul>
        </div>
    </nav>
    </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

		  <!-- Sidebar user panel -->
			<div class="user-panel">
				<div class="pull-left image">
					<img src="<?php  echo base_url();?>assets/admin/dist/img/avatar.png" class="img-circle" alt="User Image">
				</div>
				<div class="pull-left info">
					<p> <?php echo @$this->session->userdata('user_name'); ?></p>
					<i class="fa fa-circle text-success"></i> オンライン
				</div>
			</div>

			<?php $current= $current=$this->uri->segment(2);

				if(in_array($current,array('users'))) {
				$humpro='active';
			}

				if(in_array($current,array('transactions'))) {
				$transaction='active';
			}

			@$user=$this->session->all_userdata();

			?>

		  <!-- sidebar menu: : style can be found in sidebar.less -->
			<ul class="sidebar-menu">
				<li class="header">メインナビゲーション</li>

				<li class="treeview <?php echo @$transaction; ?>">
					<a href="<?php  echo base_url();?>admin/posts"><i class="fa fa-cloud-upload"></i> <span>ユーザーのメッセー</span></a>
				</li>
				<!--<li class="treeview <?php echo @$transaction; ?>">-->
				<!--	<a href="<?php  echo base_url();?>admin/comments"><i class="fa  fa-commenting"></i> <span>コメント</span></a>-->
				<!--</li>-->
				
				<li class="treeview <?php echo @$transaction; ?>">
					<a href="<?php  echo base_url();?>admin/users"><i class="fa fa-fw fa-users"></i> <span>ユーザー</span></a>
				</li>
				<li class="treeview <?php echo @$transaction; ?>">
					<a href="<?php  echo base_url();?>admin/updatepass"><i class="fa fa-key"></i> <span>パスワードの変更</span></a>
				</li>
				<li class="treeview">
					<a href="<?php  echo base_url();?>admin/logout">  <i class="fa fa-fw fa-sign-out"></i> <span>ログアウト</span> </a>
				</li>
			</ul> <!--Main ui for admin-->

        </section>
        <!-- /.sidebar -->
      </aside>

	  <?php date_default_timezone_set('EST'); ?>
