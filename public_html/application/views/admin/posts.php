<?php $this->load->view('admin/header'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
		<div class='del_res'>
		<?php
		if(isset($succes_message))
{
	
	echo '<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                  '.$succes_message.'
                  </div>';
}	
if(isset($error_message))
{
	echo '<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                  '.$error_message.'
                  </div>';
}

?>
</div>
        <section class="content-header">
          <h1>
            <?php
              echo "ユーザーのメッセー";
            ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php  echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> ホーム </a></li>
            <li class="active">ユーザーのメッセー</li>
          </ol>
        </section>

     
		    <!-- Main content -->
        <section class="content">
          <div class="row">
           
		      <div class="row">
            <div class="col-md-12">
				      <div class="box">
                <div class="box-header">
                  <!-- <h3 class="box-title">List of Booking</h3> -->
                  <!-- Export Data -->
                  <input type="submit" class="btn btn-danger delete-row" name="" style="float:right" value="削除"/>
                  <!-- <a class="btn btn-success" href='<?= base_url() ?>index.php/admin/<?= $type; ?>_exportCSV' style="float:right; margin-right:10px;">Export to CSV</a> -->
                </div><!-- /.box-header -->
    
                <div class="box-body table-responsive">
                  <table id="example1" class="table table-bordered table-striped" >
                     <thead>
                        <tr>
                             <th style="width:20px"><input type="checkbox" id="select_all" value=""/></th>
                            <th>号</th>
                            <th>ユーザーのメッセー ID</th>
                            <th>ユーザー名</th>
                            <th>ユーザー写真</th>
                            <th>内容</th>
                            <th>日付</th>
                        </tr>
                    </thead>
                   
                    <tbody>
                  <?php $i = 1; foreach($posts as $value) { ?>
                      <tr id='<?php echo $value->id; ?>'>
                        <td><input type="checkbox" name="chcked_id[]" class="checkbox" data-id="<?php echo @$value->id; ?>"/></td>
                          <td><?php echo $i; ?></td>
                          <td><?php echo $value->id; ?></td>
                          <td>
                            <a href="<?php  echo base_url();?>admin/user-detail/<?= $value->user_id; ?>">
                                  <?php echo $value->user_name; ?>
                            </a>
                          </td>

                          <td class="col-md-2 col-lg-2 col-xs-2" ><img width="auto" style="max-height: 150px" src=<?php echo $value->user_photo;?> />
                          </td>

                          <td  class="col-md-6"><?php echo $value->post_content; ?>
                          </td>
                          
                          <td class="col-md-2"><?php echo $value->created_at; ?></td>
                      </tr>
                  <?php $i++; }?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
		
      </div><!-- /.content-wrapper -->


      <script>
    $(document).ready(function() {
      $('#select_all').on('click', function() {
        if (this.checked) {
          $('.checkbox').each(function() {
            this.checked = true;
          });
        } else {
          $('.checkbox').each(function() {
            this.checked = false;
          });
        }
      });

      $('.checkbox').on('click', function() {
        if($('.checkbox:checked').length == $('.checkbox').length) {
          $('#select_all').prop('checked', true);
        } else {
          $('#select_all').prop('checked', false);
        }
      });

      $('.approve_checkbox').on('click', function() {
        if (this.checked) {
          var result = confirm("Are you sure want to approve?");
          if (result) {
            $.post( baseurl + "updateapprove", { id: $(this).attr('data-id'), approve: 1 } );
          } else {
            this.checked = false;
          }
        } else {
          var result = confirm("Are you sure want to disapprove?");
          if (result) {
            $.post( baseurl + "updateapprove", { id: $(this).attr('data-id'), approve: 0 } );
          } else {
            this.checked = true;
          }
        }
      });



      $('.delete-row').on('click', function() {
        if ($('.checkbox:checked').length > 0) {

          var allVals = [];  
          $(".checkbox:checked").each(function() {  
              allVals.push($(this).attr('data-id'));
          });

          var result = confirm("Are you sure to delete selected post?");
          if (result) {
            var join_selected_values = allVals.join(","); 

            $.ajax({
              url : baseurl+'delete_post',
              method : 'POST',
              data: 'ids='+join_selected_values,
              success: function (data) {
                console.log(data);
                $(".checkbox:checked").each(function() {  
                    $(this).parents("tr").remove();
                });
                alert("Item Deleted successfully.");
              },
              error: function (data) {
                  alert(data.responseText);
              }
            });
          }
        } else {
          alert('Select at least 1 record to delete.');
        }
      });

    });

   </script>
	 
  <?php $this->load->view('admin/footer'); ?>
  
 