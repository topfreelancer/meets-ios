
<!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Emoglass</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="description" content="learnme" />
	<meta name="author" content="learnme.us" />

	<link href="https://fonts.googleapis.com/css?family=Roboto+Mono:300,400" rel="stylesheet">
	<link rel="icon" href="/assets/admin/images/fav.png" sizes="32x32">
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/dist/css/animate.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/bootstrap/css/bootstrap.min.css">
	<!-- Style -->
	<link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/dist/css/landing_style.css">
	<link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/dist/css/landing_style1.css">

	<!-- Modernizr JS -->
	<script src="assets/admin/dist/js/modernizr-2.6.2.min.js"></script>

	</head>
	<body>
	<div>
	    <div class="row">
			<div class="col-md-12 text-center" style="margin-top:3vh">
			    <a href="/" class="animate-box">
			        <img src="assets/admin/images/logo.png" style="width:15vh">
			    </a>
			</div>
		</div>
	    <div class="row" style="">
    	     <div class="col-md-xd-1"></div>
    	     <div class="col-md-xd-10 text-center" style="">
    	         <div class="call-to-action" style="float:left;margin-left: 50px">
					<!--<a href="/" class="animate-box">-->
					<!--	<img src="assets/admin/images/logo.png" style="width:15vh">-->
					<!--</a>-->
				</div>
    	        <h1 class="animate-box" style="margin-top:2vh;margin-bottom: 10px; font-family: Daytona">
    	            <span> Emoglass 
    	            <br />いつでも、どこでも、世界中。</span></h1>
	        </div>
	        <div class="col-md-xd-1"></div>
	   </div>
	    <div class="row" style="margin-top:1vh;margin-bottom:5vh">
	        <div class="col-md-4">
			</div>
			<div class="col-md-4" style="margin:0 auto">
			    
			</div>
			<div class="col-md-4">
			    <div class="row">
			        <div class="col-md-12">
        				<div class="row" style="float:center;margin-top:-4px">
        					<div class="col-md-12 section-heading text-center" style="padding-bottom:0px">
        					    <div>
        							<a href="#" >
        								<img src="assets/admin/images/app_store.png" style="width:18vh">
        							</a>
        							<a href="#" >
        								<img src="assets/admin/images/play_store.png" style="width:18vh">
        							</a>
        						</div>
        					</div>
        				</div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
  <div class="about-carousel clear">
    <ul class="about-carousel__list about-carousel__list--sliding  row">
      </li><li class="about-carousel__item about-carousel__item--push-up">
        <a href="/">
          <img alt="Splash Screen" src="assets/admin/images/Screenshot_1.png">
          <span>Splash Screen</span>
        </a>
      </li>
      </li><li class="about-carousel__item about-carousel__item--push-up">
        <a href="/">
          <img alt="Login Screen" src="assets/admin/images/Screenshot_2.png">
          <span>Login Screen</span>
        </a>
      </li>
      </li><li class="about-carousel__item about-carousel__item--push-up">
        <a href="/">
          <img alt="Sign up Screen" src="assets/admin/images/Screenshot_3.png">
          <span>Sign up Screen</span>
        </a>
      </li>
      </li><li class="about-carousel__item about-carousel__item--push-up">
        <a href="/">
          <img alt="Terms Screen" src="assets/admin/images/Screenshot_4.png">
          <span>Terms Screen</span>
        </a>
      </li>
      </li><li class="about-carousel__item about-carousel__item--push-up">
        <a href="/">
          <img alt="Privacy Screen" src="assets/admin/images/Screenshot_5.png">
          <span>Privacy Screen</span>
        </a>
      </li>
      </li><li class="about-carousel__item about-carousel__item--push-up">
        <a href="/">
          <img alt="Forgot Screen" src="assets/admin/images/Screenshot_6.png">
          <span>Forgot Screen</span>
        </a>
      </li>
      </li><li class="about-carousel__item about-carousel__item--push-up">
        <a href="/">
          <img alt="Home Screen" src="assets/admin/images/Screenshot_7.png">
          <span>Home Screen</span>
        </a>
      </li>
      </li><li class="about-carousel__item about-carousel__item--push-up">
        <a href="/">
          <img alt="Detail Screen" src="assets/admin/images/Screenshot_8.png">
          <span>Detail Screen</span>
        </a>
      </li>
      </li><li class="about-carousel__item about-carousel__item--push-up">
        <a href="/">
          <img alt="Comment Screen" src="assets/admin/images/Screenshot_9.png">
          <span>Comment Screen</span>
        </a>
      </li>
      </li><li class="about-carousel__item about-carousel__item--push-up">
        <a href="/">
          <img alt="Search Screen" src="assets/admin/images/Screenshot_10.png">
          <span>Search Screen</span>
        </a>
      </li>
      </li><li class="about-carousel__item about-carousel__item--push-up">
        <a href="/">
          <img alt="Creat Post Screen" src="assets/admin/images/Screenshot_11.png">
          <span>Creat Post Screen</span>
        </a>
      </li>
    </li><li class="about-carousel__item about-carousel__item--push-up">
        <a href="/">
          <img alt="Profile Screen" src="assets/admin/images/Screenshot_12.png">
          <span>Profile Screen</span>
        </a>
      </li>
    </li><li class="about-carousel__item about-carousel__item--push-up">
        <a href="/">
          <img alt="Settings Screen" src="assets/admin/images/Screenshot_13.png">
          <span>Settings Screen</span>
        </a>
      </li>
    </li><li class="about-carousel__item about-carousel__item--push-up">
        <a href="/">
          <img alt="Edit Profile Screen" src="assets/admin/images/Screenshot_14.png">
          <span>Edit Profile Screen</span>
        </a>
      </li>
    </li><li class="about-carousel__item about-carousel__item--push-up">
        <a href="/">
          <img alt="Change Password Screen" src="assets/admin/images/Screenshot_15.png">
          <span>Change Password Screen</span>
        </a>
      </li>
    </ul>
  </div>
  <div class="call-to-action" style="text-align:center">
        <div class="row">
            <div class="col-md-6" style="text-align:right" >
                <a href="<?php  echo base_url("privacy");?>">
        		    <img src="assets/admin/images/privacy.jpg" style="width:14vh">
        	    </a>
            </div>
            <div class="col-md-6" style="text-align:left">
                <a href="<?php  echo base_url("terms");?>">
        		    <img src="assets/admin/images/terms.png" style="width:14vh">
        	    </a>
            </div> 
        </div>
  </div>
  
  
	<!-- jQuery -->
	<script src="<?php  echo base_url();?>assets/admin/dist/js/jquery-1.12.0.min.js"></script>
	<!-- jQuery Easing -->
	<!-- Bootstrap -->
	<script src="<?php  echo base_url();?>assets/admin/bootstrap/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="assets/admin/dist/js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="assets/admin/dist/js/jquery.stellar.min.js"></script>
	<!-- Counters -->
	<!-- Main JS (Do not remove) -->
	<script src="assets/admin/dist/js/main.js"></script>

	</body>
</html>

