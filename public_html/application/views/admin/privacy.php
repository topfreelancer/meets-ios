
<!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>MeetsApp</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="MeetsApp" />
	<meta name="author" content="Meets.email" />

	<link href="https://fonts.googleapis.com/css?family=Roboto+Mono:300,400" rel="stylesheet">
	<link rel="icon" href="/assets/admin/images/fav.png" sizes="32x32">
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/dist/css/animate.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/bootstrap/css/bootstrap.min.css">
	<!-- Style -->
	<link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/dist/css/landing_style.css">

	<!-- Modernizr JS -->
	<script src="assets/admin/dist/js/modernizr-2.6.2.min.js"></script>

	</head>
	<body>
	<header role="banner" id="fh5co-header">
		<div class="container">
			<div class="row">
				<nav class="navbar navbar-default navbar-fixed-top" style="position: inherit;">
					<div class="navbar-header">
						<!-- Mobile Toggle Menu Button -->
						<!-- <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a> -->

						<div class="col-md-4">
							<div class="row">
								<!--<a class="navbar-brand" href="#">learnme</a> -->
								<a href="#" class="animate-box" style="display: block; margin-left: 20px; margin-top: 15px;">
									<img src="assets/admin/images/logo.png" style="width:100px">
								</a>
							</div>
						</div>
					</div>
				</nav>
			</div>
	  </div>
	</header>

	<section id="fh5co-blog" data-section="blog">
		<div class="fh5co-blog">
			<div class="container" style="width: 100%">
				<div class="row">
					<div class="col-md-12 section-heading">

						<h2 class="animate-box text-center" style="font-weight: bold;margin-top: 20px;"><span>個人情報保護方針</span></h2>
						<div class="row">
							<div class="col-md-8 col-md-offset-2 subtext animate-box" > <!-- style="word-break: break-all;"-->

<span style="margin-left: 20px">1.	</span>
<u style="font-weight: bold; font-size: 17px;">個人情報の取得について</u>
<p style="margin-left: 30px;">
<span>・ 個人情報の取得は適法かつ公正な手段によって行います。</span><br>
</p>

<span style="margin-left: 20px">2.	</span>
<u style="font-weight: bold; font-size: 17px;">個人情報の利用について</u>
<p style="margin-left: 30px;">
<span>・ 取得する個人情報の利用目的をできるだけ特定し明らかにします。</span><br>
<span>・ 個人情報の利用は、利用目的の範囲内で、具体的な業務に応じて権限を有する者が、業務上必要な範囲内で行います。</span><br>
</p>

<span style="margin-left: 20px">3.	</span>
<u style="font-weight: bold; font-size: 17px;">個人情報の第三者への開示・提供について</u>
<p style="margin-left: 30px;">
<span>・ 以下の場合を除き、ご本人の同意を得ることなく個人情報を第三者に開示・提供することはいたしません。</span><br>
</p>

<span style="margin-left: 20px">4.	</span>
<u style="font-weight: bold; font-size: 17px;">個人情報の管理(安全管理措置)について</u>
<p style="margin-left: 30px;">
<span>・ 個人情報に対する不正アクセス、個人情報の紛失、改ざん、漏洩などを防止するため、適切な安全対策を講じます。</span><br>
<span>・ 個人情報の取扱いを委託する場合は、委託先と安全管理措置の内容を含む適切な契約を締結し、委託先に対する管理・監督を徹底するなど必要な措置を講じます。</span><br>
<span>・ 個人情報の取扱いに関する規定を定め着実に実行するとともに、継続的に改善していきます。</span><br>

</p>
							</div>
						</div>

						<!-- <div class="call-to-action" style="margin-top:10vh">
							<a href="#" class="animate-box" target="_blank">
								<img src="assets/admin/images/app_store.png" style="width:22vh">
							</a>
							<a href="#" class="animate-box" target="_blank">
								<img src="assets/admin/images/play_store.png" style="width:22vh">
							</a>
						</div> -->
					</div>
				</div>
				
			</div>
		</div>
	</section>
	
	<!-- jQuery -->
	<script src="<?php  echo base_url();?>assets/admin/dist/js/jquery-1.12.0.min.js"></script>
	<!-- jQuery Easing -->
	<!-- Bootstrap -->
	<script src="<?php  echo base_url();?>assets/admin/bootstrap/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="assets/admin/dist/js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="assets/admin/dist/js/jquery.stellar.min.js"></script>
	<!-- Counters -->
	<!-- Main JS (Do not remove) -->
	<script src="assets/admin/dist/js/main.js"></script>

	</body>
</html>

