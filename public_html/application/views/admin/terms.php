
<!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>MeetsApp</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="meets" />
	<meta name="author" content="meets.email" />

	<link href="https://fonts.googleapis.com/css?family=Roboto+Mono:300,400" rel="stylesheet">
	<link rel="icon" href="/assets/admin/images/fav.png" sizes="32x32">
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/dist/css/animate.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/bootstrap/css/bootstrap.min.css">
	<!-- Style -->
	<link rel="stylesheet" href="<?php  echo base_url();?>assets/admin/dist/css/landing_style.css">

	<!-- Modernizr JS -->
	<script src="assets/admin/dist/js/modernizr-2.6.2.min.js"></script>

	</head>
	<body>
	<header role="banner" id="fh5co-header">
		<div class="container">
			<div class="row">
				<nav class="navbar navbar-default navbar-fixed-top" style="position: inherit;">
					<div class="navbar-header">
						<!-- Mobile Toggle Menu Button -->
						<!-- <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a> -->

						<div class="col-md-4">
							<div class="row">
								<!--<a class="navbar-brand" href="#">learnme</a> -->
								<a href="#" class="animate-box" style="display: block; margin-left: 20px; margin-top: 15px;">
									<img src="assets/admin/images/logo.png" style="width:100px">
								</a>
							</div>
						</div>
					</div>
				</nav>
			</div>
	  </div>
	</header>

	<section id="fh5co-blog" data-section="blog">
		<div class="fh5co-blog">
			<div class="container" style="width: 100%">
				<div class="row">
					<div class="col-md-12 section-heading">

						<h2 class="animate-box text-center" style="font-weight: bold;margin-top:20px;"><span>利用規約</span></h2>
						<div class="row">
							<div class="col-md-8 col-md-offset-2 subtext animate-box" > <!-- style="word-break: break-all;"-->
								
<span style="margin-left: 20px">1.	</span>
<u style="font-weight: bold; font-size: 17px;">規約への同意</u>
<p style="margin-left: 30px;">
<span>・ お客様は、本規約の定めに従って本サービスを利用しなければなりません。お客様は、本規約に同意をしない限り本サービスを利用できません。</span><br>
</p>

<span style="margin-left: 20px">2.	</span>
<u style="font-weight: bold; font-size: 17px;">規約の変更</u>
<p style="margin-left: 30px;">
<span>・ MeetsAppは、MeetsAppが必要と判断する場合、本サービスの目的の範囲内で、本規約を変更することができます。その場合、MeetsAppは、変更後の本規約の内容および効力発生日を、本サービスに表示し、またはMeetsAppが定める方法によりお客様に通知することでお客様に周知します。変更後の本規約は、効力発生日から効力を生じるものとします。</span><br>
</p>

<span style="margin-left: 20px">3.	</span>
<u style="font-weight: bold; font-size: 17px;">お客様の責任</u>
<p style="margin-left: 30px;">
<span>・ お客様は、お客様ご自身の責任において本サービスを利用するものとし、本サービスにおいて行った一切の行為およびその結果について一切の責任を負うものとします。</span><br>
<span>・ MeetsAppは、お客様が以下のいずれかに該当する場合または該当するおそれがある場合、あらかじめお客様に通知することなく、本サービスの全部または一部の利用の停止、アカウントの停止または削除、本サービスに関するお客様と当社との間の契約（本規約に基づく契約を含みます。以下同じ。）の解除その他の当社が必要かつ適切と合理的に判断する措置を講じることができます。</span><br>
<span>(1) 適用のある法令または本規約もしくは個別利用条件に違反した場合</span><br>
<span>(2) 反社会的勢力の構成員またはその関係者である場合</span><br>
<span>(3) 風説の流布、偽計、威力その他の不正な手段を用いて当社の信用を毀損する場合</span><br>
<span>(4) (1)から(3)までに定めるもののほか、お客様との信頼関係が失われた場合その他お客様への本サービスの提供が適切でないと当社が合理的に判断した場合</span><br>
<span>・ お客様は、本サービスを利用したことに起因して（MeetsAppがかかる利用を原因とするクレームを第三者より受けた場合を含みます。）、MeetsAppが直接的または間接的に何らかの損害（弁護士費用の負担を含みます。）を被った場合、MeetsAppの請求にしたがって直ちにこれを賠償しなければなりません。</span><br>
</p>

<span style="margin-left: 20px">4.	</span>
<u style="font-weight: bold; font-size: 17px;">非保証</u>
<p style="margin-left: 30px;">
<span>・ MeetsAppは、本サービス（本コンテンツを含みます。）に関する瑕疵（セキュリティ等に関する欠陥、エラーやバグ、権利侵害を含みます。）がないこと、ならびに安全性、信頼性、正確性、完全性、有効性および特定の目的への適合性を明示的にも黙示的にも保証していません。MeetsAppは、お客様に対して、かかる瑕疵を除去して本サービスを提供する義務を負いません。</span><br>
</p>

<span style="margin-left: 20px">5.	</span>
<u style="font-weight: bold; font-size: 17px;">禁止事項</u>
<p style="margin-left: 30px;">
<span>・ MeetsAppは本サービスに関するお客様による以下の行為を禁止します。 </span><br>
<span>・ 法令、裁判所の判決、決定もしくは命令、または法令上拘束力のある行政措置に違反する行為</span><br>
<span>・ 公の秩序または善良の風俗に反するおそれのある行為</span><br>
<span>・ MeetsAppまたは第三者の著作権、商標権、特許権等の知的財産権、名誉権、プライバシー権、その他法令上または契約上の権利を侵害する行為</span><br>
<span>・ 過度に暴力的な表現、露骨な性的表現、児童ポルノ・児童虐待に相当する表現、人種、国籍、信条、性別、社会的身分、門地等による差別につながる表現、自殺、自傷行為、薬物乱用を誘引または助長する表現、その他反社会的な内容を含み他人に不快感を与える表現を、投稿または送信する行為 </span><br>
<span>・ MeetsAppまたは第三者になりすます行為または意図的に虚偽の情報を流布させる行為 </span><br>
<span>・ 反社会的勢力に対する利益供与その他の協力行為 </span><br>
<span>・ 営業、宣伝、広告、勧誘、その他営利を目的とする行為（MeetsAppの認めたものを除きます。）、性行為やわいせつな行為を目的とする行為、面識のない第三者との出会いや交際を目的とする行為、他のお客様に対する嫌がらせや誹謗中傷を目的とする行為、その他本サービスが予定している利用目的と異なる目的で本サービスを利用する行為  </span><br>
<span>・ 宗教活動または宗教団体への勧誘行為 </span><br>
<span>・ 他人の個人情報、登録情報、利用履歴情報等を、不正に収集、開示または提供する行為</span><br>
<span>・ 本サービスのサーバやネットワークシステムに支障を与える行為、BOT、チートツール、その他の技術的手段を利用して本サービスを不正に操作する行為、本サービスの不具合を意図的に利用する行為、同様の質問を必要以上に繰り返す等、当社に対し不当な問い合わせまたは要求をする行為、その他当社による本サービスの運営または他のお客様による本サービスの利用を妨害し、これらに支障を与える行為</span><br>
<span>・ MeetsAppは、お客様が投稿コンテンツに関し法令もしくは本規約に違反し、または違反するおそれがあると認めた場合、その他業務上合理的な必要がある場合、あらかじめお客様に通知することなく、投稿コンテンツを削除する等の方法により、本サービスの利用を制限できます。</span><br>
</p>



							</div>
						</div>

						<!-- <div class="call-to-action" style="margin-top:10vh">
							<a href="#" class="animate-box" target="_blank">
								<img src="assets/admin/images/app_store.png" style="width:22vh">
							</a>
							<a href="#" class="animate-box" target="_blank">
								<img src="assets/admin/images/play_store.png" style="width:22vh">
							</a>
						</div> -->
					</div>
				</div>
				
			</div>
		</div>
	</section>
	
	<!-- jQuery -->
	<script src="<?php  echo base_url();?>assets/admin/dist/js/jquery-1.12.0.min.js"></script>
	<!-- jQuery Easing -->
	<!-- Bootstrap -->
	<script src="<?php  echo base_url();?>assets/admin/bootstrap/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="assets/admin/dist/js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="assets/admin/dist/js/jquery.stellar.min.js"></script>
	<!-- Counters -->
	<!-- Main JS (Do not remove) -->
	<script src="assets/admin/dist/js/main.js"></script>

	</body>
</html>

