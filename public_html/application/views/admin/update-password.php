<?php $this->load->view('admin/header'); ?>
   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
<?php 
if(isset($succes_message))
{
	
	echo '<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                  '.$succes_message.'
                  </div>';
}	

 if(@$error_message!='')
{
	echo '<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                  '.$error_message.'
                  </div>';
}

?>


        <section class="content-header">
          <h1>
            管理者パスワードの変更
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php  echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> ホーム</a></li>
            <li class="active">パスワードの変更</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- Post News -->
              <div class="box box-primary">
                <!-- form start -->
                <form role="form" method='post' enctype="multipart/form-data" action='<?php  echo base_url();?>admin/updatepass/<?php echo $admin_id; ?>' >
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">新しいパスワードを入力する</label>
                      <input type="text" class="form-control" id=""  name='password' placeholder="パスワードを入力する" value='' required >
                    </div>
				 <div class="form-group">
                      <label for="exampleInputEmail1">新しいパスワードを再入力する</label>
                      <input type="text" class="form-control" id=""  name='re-password' placeholder="パスワード再入力" value='' required >
					  <div class='msg-error'><?php echo validation_errors(); ?></div>
		</div>
                    </div>
					
                  <div class="box-footer">
                    <input type="submit" class="btn btn-primary" name='submit' value='変更'></button>
                  </div>
				   
                </form>
              </div><!-- /.box -->
            <!-- right column -->
			
	</div>
		
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  

  
	
 <?php $this->load->view('admin/footer'); ?>
  
  