  <?php $this->load->view('admin/header'); ?>

 <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            ダッシュ ボード
            <small>コントロールパネル</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php  echo base_url();?>/admin/dashboard"><i class="fa fa-dashboard"></i> ホーム</a></li>
            <li class="active">ダッシュ ボード</li>
          </ol>
        </section>


    <section class="content">
     <div class="row">
       <!-- ./col -->

       <div class="col-lg-6 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3><?php  echo $user_count;?></h3>

              <p>ユーザー</p>
            </div>
            <div class="icon">
              <i class="fa fa-user"></i>
            </div>
            <a href="<?php  echo base_url();?>admin/users" class="small-box-footer">詳しくは <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        
        <div class="col-lg-6 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php  echo $post_count;?></h3>

              <p>合計投稿</p>
            </div>
            <div class="icon">
              <i class="fa fa-cloud-upload"></i>
            </div>
            <a href="<?php  echo base_url();?>admin/posts" class="small-box-footer">詳しくは <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
      </div>

    </section>


       <!-- /.content -->
     
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>バージョン</b> 1.0
        </div>
        <strong>著作権 &copy; <?php echo date("Y") ?> <a href="#">MeetsApp</a></strong> 全著作権所有
      </footer>

    
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php  echo base_url();?>assets/admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php  echo base_url();?>assets/admin/bootstrap/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
  
    <!-- Sparkline -->
    <script src="<?php  echo base_url();?>assets/admin/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="<?php  echo base_url();?>assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?php  echo base_url();?>assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  
 
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php  echo base_url();?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php  echo base_url();?>assets/admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php  echo base_url();?>assets/admin/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php  echo base_url();?>assets/admin/dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!--script src="<?php  echo base_url();?>assets/admin/dist/js/pages/dashboard.js"></script-->
    <!-- AdminLTE for demo purposes -->
    <script src="<?php  echo base_url();?>assets/admin/dist/js/demo.js"></script>
  </body>
</html>
