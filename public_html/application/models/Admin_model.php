<?php
class Admin_model extends CI_Model{
	
	function __construct() {
		parent::__construct();
	}

	function get_recomended()
	{
		$queryString = "SELECT tb_c.id AS category_id, tb_c.title AS title, tb_r.id AS recomended_id, tb_r.title AS subtitle FROM `tb_categories` tb_c LEFT JOIN `tb_recomended` AS tb_r ON tb_c.id = tb_r.category_id ORDER BY tb_c.id";
		$query = $this->db->query($queryString);
		return $query->result();
	}

	function get_count($table_name = '', $where=array()){

        $rs = $this->db->get_where($table_name,$where);
        return $rs->num_rows();
    }

	
	function insert_query($tb,$data){

		$this->db->insert($tb,$data);
	}

	function get_values($tb){

		$this->db->order_by("id","desc");
		$query = $this->db->get($tb);
		return $query->result();
	}
	
	function get_values_asc($tb) {

		$this->db->order_by("id","asc");
		$query = $this->db->get($tb);
		return $query->result();

	}
	function get_values_orderby($tb, $order){

		$this->db->order_by($order,"asc");
		$query = $this->db->get($tb);
		return $query->result();
	}
	function get_single_value($tb,$id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($tb);
		return $query->result();
	}
	function get_single_value_other_condition($tb,$cond)
	{
		$this->db->where($cond);
		$query = $this->db->get($tb);
		return $query->result();
	}
	function update_values($tb,$id,$data)
	{
		$this->db->where('id',$id);
		$this->db->update($tb,$data);
	} 

	function delete_value($tb, $condition)
	{
		$this->db->where($condition);
		$this->db->delete($tb); 
	}

	function delete_all($tb, $ids)
	{
		$in = explode(',', $ids);

		$this->db->select("*");
		$this->db->where_in('id', $in);
		
		$this->db->delete($tb); 
	}
	function  get_condtion($tb,$cond)
	{
		$this->db->order_by("id","desc");
		$this->db->where($cond);
		$query = $this->db->get($tb);
		return $query->result(); 

	} 
	function  get_condtion_asc($tb,$cond)
	{
		//$this->db->order_by("id","asc");
		$this->db->where($cond);
		$query = $this->db->get($tb);
		return $query->result(); 

	}
	function  get_condtion_orderby($tb,$cond,$colm,$order)
	{
		$this->db->order_by($colm,$order);
		$this->db->where($cond);
		$query = $this->db->get($tb);
		return $query->result(); 

	}	
	function  get_condtion_like($tb,$cond)
	{
		$this->db->like($cond);
		$query = $this->db->get($tb);
		return $query->result(); 

	} 

	function count_tb_record($tb){
		return $this->db->count_all($tb);
	}

	function  get_condtion_count($tb,$cond)
	{
		$this->db->where($cond);
		$query = $this->db->get($tb);
		return $query->num_rows(); 

	} 

	function get_values_limited($tb,$limit,$start){

		$this->db->order_by("id","DESC");
		$this->db->limit($limit, $start);
		$query = $this->db->get($tb);
		return $query->result();

	}

	function get_values_limit_cond($tb,$col,$orderby,$limit,$start){

		$this->db->order_by($col,$orderby);
		$this->db->limit($limit, $start);

		$query = $this->db->get($tb);
		return $query->result();

	}

	function get_values_limit_where($tb,$cond,$col,$orderby,$limit,$start){

		$this->db->order_by($col,$orderby);
		$this->db->limit($limit, $start);
		$this->db->where($cond);
		$query = $this->db->get($tb);
		return $query->result();

	}
	function where_in($tb,$cond)
	{
		$in= explode(',',$cond);

		$this->db->select("*");
		$this->db->where_in('id',$in);

		$query =$this->db->get($tb);
		return $query->result();
	}

	//email esixtence
	function isEmailExist($email) {
		
		$this->db->select('id');
		$this->db->where('email', $email);
		$query = $this->db->get('tb_user');

		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	function isExistvalue($tb, $cond) {
		
		$this->db->select('id');
		$this->db->where($cond);
		$query = $this->db->get($tb);

		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
}