<?php

class Api_model extends CI_Model {

  function __construct(){
    parent::__construct();
  }


  function emailExist($email) {

    $this->db->where('email', $email);
    return $this->db->get('tb_user')->num_rows();
  }
  
  function usernameExist($username) {

    $this->db->where('username', $username);
    return $this->db->get('tb_user')->num_rows();
  }
  
  function phoneExist($phone) {

    $this->db->where('phone', $phone);
    return $this->db->get('tb_user')->num_rows();
  }

  function register($username, $password_hash, $token, $picture, $aboutme, $userlocation, $userbirthday, $lat, $lon, $usergender) {

    $this->db->set('username', $username);
    $this->db->set('password', $password_hash);
    $this->db->set('picture', $picture);
    $this->db->set('follower', 0);
    $this->db->set('notification', 1);// user can get notification with default state, if 0, user notification state is off...
    $this->db->set('token', $token);
    $this->db->set('created_at', 'NOW()', false);
    $this->db->set('about_me', $aboutme);
    $this->db->set('user_location', $userlocation);
    $this->db->set('user_birthday', $userbirthday);
    $this->db->set('latitude', $lat);
    $this->db->set('longitude', $lon);
    $this->db->set('user_gender', $usergender);
    $this->db->insert('tb_user');
    
    return $this->db->insert_id();
  }
  
  function registerOther ($userid, $userattr, $userpurpose) {
      
     if(strlen($userattr)>0){
        $attr_array = explode(",", $userattr);
        foreach($attr_array as $one){
            $this->db->set('user_id', $userid);
            $this->db->set('attribute', $one);
            $this->db->insert('tb_attr');
        }
    }
    if(strlen($userpurpose)>0){
        $purpose_array = explode(",", $userpurpose);
        foreach($purpose_array as $one){
            $this->db->set('purpose_user_id', $userid);
            $this->db->set('purpose', $one);
            $this->db->insert('tb_purpose');
        }
    }
  }
  
  

  function insert_query($tb,$data){

    $this->db->insert($tb,$data);
    return $this->db->insert_id();
  }

  function update_query($tb,$condition,$data)
  {
    $this->db->where($condition);
    $this->db->update($tb,$data);
  }

  function get_values($tb){

    $this->db->order_by("id","desc");
    $query = $this->db->get($tb);
    return $query->result();
  }
  
  function get_values_conditions($tb, $condition){

    $this->db->order_by("id","desc");
    $this->db->where($condition);
    $query = $this->db->get($tb);
    return $query->result();
  }
  
  function delete_value($tb, $condition)
  {
    $this->db->where($condition);
    $this->db->delete($tb); 
  }

  function setPassword($username, $password) {

    $this->db->where('username', $username);
    $this->db->set('password', $password);
    $this->db->update('tb_user');
  }
  function changePassword($id, $password) {

    $this->db->where('id', $id);
    $this->db->set('password', $password);
    $this->db->update('tb_user');
  }
   
  function getToken($user_id) {

    $this->db->where('id', $user_id);
    $row = $this->db->get('tb_user')->row();

    if (count($row) == 0) {
      return "";
    }

    return $row->token;
  }

  function setToken($user_id, $token) {

    $this->db->where('id', $user_id);
    $this->db->set('token', $token);
    $this->db->update('tb_user');
  }
  
  function setToken1($user_id, $token, $joined_date, $is_plus_member) {
    $this->db->where('user_id', $user_id);
    $row = $this->db->get('tb_user1')->row();

    if (count($row) == 0) {
        $this->db->set('user_id', $user_id);
        $this->db->set('token', $token);
        $this->db->set('joined_date', $joined_date);
        $this->db->set('is_plus_member', $is_plus_member);
        $this->db->insert('tb_user1');
    }else{
        $this->db->where('user_id', $user_id);
        $this->db->set('token', $token);
        $register_joined_date = $row->joined_date;
        if ($register_joined_date == ''){
            $this->db->set('joined_date', $joined_date);
        }
        $this->db->set('is_plus_member', $is_plus_member);
        $this->db->update('tb_user1');
    }
  }
  
   function makeRandomCode(){

    $random_code = '';

    $arr = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U',
    'V','W','X','Y','Z','1','2','3','4','5','6','7','8','9','0','a','b','c','d','e','f',
    'g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');

    for ($i = 0 ; $i < 30 ; $i++) {

      $index = mt_rand(0, count($arr) - 1);
      $random_code .= $arr[$index];
    }

    return $random_code;
  }
}
?>
