<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


require_once 'vendor/autoload.php';

//********* Authorize.net Payment gateway **********//
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
define("AUTHORIZENET_LOG_FILE", "phplog");

use paragraph1\phpFCM\Client;
use paragraph1\phpFCM\Message;
use paragraph1\phpFCM\Recipient\Device;
use paragraph1\phpFCM\Notification;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Api extends CI_Controller  {

	var $FCM_APIKEY;

	function __construct(){

		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('api_model');
		$this->load->library('session');
		$this->load->library('apn');
		$this->FCM_APIKEY  = 'AAAA8D3HcK8:APA91bEh-f6upZBWtTxDFdWUcWCNI95wxtoXoPh5NtEng3lbjq0Vv37s_oPu4RL8piLv2ODK9VRPKm6z_6ezbDySpRZnhnCrL9g5bYFrCjwcJ1oC5eiv5f6RFC-przXFcfiwG-bnfuzF';
		$this->load->library('image_lib');
		$this->load->library('zip');
		date_default_timezone_set('Asia/Tokyo');
		
		if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
        // Ignores notices and reports all other kinds... and warnings
        error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
        // error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
        }

	}

/**
* Make json response to the client with result code message
*
* @param p_result_code : Result code
* @param p_result_msg : Result message
* @param p_result : Result json object
*/

private function doRespond($p_result_code,  $p_result){

	$p_result['result_code'] = $p_result_code;

	$this->output->set_content_type('application/json')->set_output(json_encode($p_result));
}

/**
* Make json response to the client with success.
* (result_code = 0, result_msg = "success")
*
* @param p_result : Result json object
*/

private function doRespondSuccess($result){

	$this->doRespond(200, $result);
}

private function doUrlDecode($p_text){

	$p_text = urldecode($p_text);

	$p_text = str_replace('&#40;', '(', $p_text);
	$p_text = str_replace('&#41;', ')', $p_text);
	$p_text = str_replace('%40', '@', $p_text);
	$p_text = str_replace('%20',' ', $p_text);
	$p_text = str_replace('%2F','/', $p_text);
	$p_text = trim($p_text);

	return $p_text;
}



function signup() {

	$result = array();

	$username = $_POST['username'];
	$password = $_POST['password'];
	$aboutme = $_POST['about_me'];
	$userlocation = $_POST['user_location'];
	$userbirthday = $_POST['user_birthday'];
	$token = $_POST['token'];
	$lat = $_POST['latitude'];
	$lon = $_POST['longitude'];
	$usergender = $_POST['user_gender'];
	$userpurpose = $_POST['purpose'];
	$userattr = $_POST['attribute'];

	$usernameExist = $this->api_model->usernameExist($username);
	if ($usernameExist > 0) {  // username already exist
		$this->doRespond(201, $result);
		return;
	}
	
	if(!function_exists('password_hash'))
	$this->load->helper('password');
	$password_hash = password_hash($password, PASSWORD_BCRYPT);


	// user photo
	$image_path = "uploadfiles/userphoto/";
	if(!is_dir($image_path)) {
		mkdir($image_path);
	}

	$image_url  = base_url().$image_path;
	$fileName = strtotime("now").rand(pow(10, 3-1), pow(10, 3)-1).'.png';

	$config = array(
		'upload_path'   => $image_path,
		'allowed_types' => '*',
		'overwrite'     => 1,
		'file_name' 	=> $fileName
	);

	$this->load->library('upload', $config);
	$this->upload->initialize($config);
	if ($this->upload->do_upload('picture')) {

		$file_url = $image_url.$this->upload->file_name;
		$userid = $result['id'] = $this->api_model->register($username, $password_hash, $token, $file_url, $aboutme, $userlocation, $userbirthday, $lat, $lon, $usergender);
		$result['picture'] = $file_url;
		// call tb_purpose, and tb_attr
		
		$this->api_model->registerOther($userid,$userattr,$userpurpose );
		$this->doRespondSuccess($result);
		
	} else {

		$this->doRespond(202, $result);// upload fail
		return;
	}
}

function signin() {

	$username = $_POST['username'];
	$password = $_POST['password'];

	$result = array();

	if(!function_exists('password_verify'))
	$this->load->helper('password');

	$username_condition = array('username' => $username);
	$this->db->where($username_condition);
	$row = $this->db->get('tb_user')->row();

	if (count($row) == 0) {
		$result['message'] = "User not exist.";
		$this->doRespond(201, $result);
		return;
	}
	$pwd = $row->password;
	if (!password_verify($password, $pwd)) {

		$result['message'] = "Incorrect Password";
		$this->doRespond(202, $result);
		return;
	}

	$token = $_POST['token'];
	$this->api_model->setToken($row->id, $token);

	$result['user_info'] =
	array('user_id' => $row->id,
	'username' => $row->username,
	'picture' => $row->picture,
	'follower' => $row->follower,
	'about_me' => $row->about_me,
	'user_location' => $row->user_location,
	'user_birthday' => $row->user_birthday );

	$this->doRespondSuccess($result);

}

function editProfile() {

	$result = array();

	$user_id = $_POST['user_id'];
	$username = $_POST['username'];
	$aboutme = $_POST['about_me'];
	$userlocation = $_POST['user_location'];
	$userbirthday = $_POST['user_birthday'];
	$latitude = $_POST['latitude'];
	$longitude = $_POST['longitude'];

	// user photo
	$image_path = "uploadfiles/userphoto/";
	if(!is_dir($image_path)) {
		mkdir($image_path);
	}
	$image_url  = base_url().$image_path;
	$fileName = strtotime("now").rand(pow(10, 3-1), pow(10, 3)-1).'.png';
	$config = array(
		'upload_path'   => $image_path,
		'allowed_types' => '*',
		'overwrite'     => 1,
		'file_name' 	=> $fileName
	);
	$this->load->library('upload', $config);
	$this->upload->initialize($config);

	$condition = array('id' => $user_id);
	if (!empty($_FILES['picture']['name'])) {
		if ($this->upload->do_upload('picture')) {
			$file_url = $image_url.$this->upload->file_name;
			$data = array('username' => $username, 'picture' => $file_url, 'about_me' => $aboutme,'user_location' => $userlocation, 'user_birthday' => $userbirthday, 'latitude' => $latitude, 'longitude' => $longitude);
			$this->api_model->update_query("tb_user", $condition, $data);
			$result['picture'] = $file_url;
			$this->doRespondSuccess($result);
		} else {
			$this->doRespond(201, $result);// upload fail
		}
	}else{
		$data = array('username' => $username, 'about_me' => $aboutme,'user_location' => $userlocation, 'user_birthday' => $userbirthday,'latitude' => $latitude, 'longitude' => $longitude,);
		$this->api_model->update_query("tb_user", $condition, $data);
		$result['picture'] = "";
		$this->doRespondSuccess($result);
	}
	
}

function set_name()
{
	$this->clean_space($_POST['name']);
}

function forgot() {

    $email = $_POST['email'];
	$username = $_POST['username'];

	$result = array();

	$is_exist = $this->api_model->usernameExist($username);

	if ($is_exist == 0) {	// username not exist
		$this->doRespond(201, $result);
		return;
	}
	$tmpPin = $this->makeRandomCode();
	$this->sendMail($email, $tmpPin);
	$result['pin_code'] = $tmpPin;
	$this->doRespondSuccess($result);
}
function reset_password() {

	$username = $_POST['username'];
	$password = $_POST['password'];

	$result = array();

	if(!function_exists('password_hash'))
	$this->load->helper('password');

	$password_hash = password_hash($password, PASSWORD_BCRYPT);
	$this->api_model->setPassword($username, $password_hash);
	$this->doRespondSuccess($result);

}
function change_password() {

	$user_id = $_POST['user_id'];
	$password = $_POST['password'];

	$result = array();

	if(!function_exists('password_hash'))
	$this->load->helper('password');

	$password_hash = password_hash($password, PASSWORD_BCRYPT);
	$this->api_model->changePassword($user_id, $password_hash);
	$this->doRespondSuccess($result);

}

/*function getTotalPost()
{
	$user_id = $_POST['user_id'];

	$result = array();
	$resultPostArray = array();

    $resultData = $this->api_model->get_values('tb_post');
    foreach($resultData as $posts) {

			$this->db->where('post_id', $posts->id);
			$this->db->where('user_id', $user_id);
			$row = $this->db->get('tb_like')->row();
			if (count($row) == 0) {
				$like = '0';
			} else{
				$like = '1';
			}
			$resultOne = array(
				'id' => $posts->id,
				'user_id' => $posts->user_id,
				'user_name' => $posts->user_name,
				'user_photo' => $posts->user_photo,
				'thumbnail' => $posts->thumbnail,
				'detailimage' => $posts->detailimage,
				'scale' => $posts->scale,
				'like' => $like
			);
			array_push($resultPostArray, $resultOne);
		}
	$result['post_info'] = $resultPostArray;
	$this->doRespondSuccess($result);
	
}*/

function getTotalPost()
{

    $user_id = $_POST['user_id'];
	$result = array();
	$resultPostArray = array();
    $resultData = $this->api_model->get_values('tb_post');
    
    foreach($resultData as $posts) {
        $owner_id=$posts->user_id;
        $this->db->where('owner_id', $owner_id);
        $this->db->where('user_id', $user_id);
    	$row = $this->db->get('tb_follow')->row();
    	if (count($row) == 0) {
            $like = '0';
        } else{
            $like = '1';
        }
        
		$resultOne = array(
			'id' => $posts->id,
			'user_id' => $posts->user_id,
			'user_name' => $posts->user_name,
			'user_photo' => $posts->user_photo,
			'created_at' => $posts->created_at,
			'post_content' => $posts->post_content,
			'user_location' => $posts->user_location,
			'user_birthday' => $posts->user_birthday,
			'follower_number' => $posts->follower_number,
			'about_me' => $posts->about_me,
			'to_send' => $posts->to_send,
			'like' => $like
		);
		array_push($resultPostArray, $resultOne);
	}
	$result['post_info'] = $resultPostArray;
	$this->doRespondSuccess($result);
	
}

function getTotalUsers()
{
    $user_id = $_POST['user_id'];
	$result = array();
	$resultPostArray = array();

    $resultData = $this->api_model->get_values('tb_user');
    
    foreach($resultData as $user) {
        $owner_id=$user->id;
        $this->db->where('owner_id', $owner_id);
        $this->db->where('user_id', $user_id);
    	$row = $this->db->get('tb_follow')->row();
    	if (count($row) == 0) {
            $like = '0';
        } else{
            $like = '1';
        }
        
		$resultOne = array(
			'id' => $user->id,
			'username' => $user->username,
			'picture' => $user->picture,
			'follower' => $user->follower,
			'created_at' => $user->created_at,
			'about_me' => $user->about_me,
			'user_location' => $user->user_location,
			'user_birthday' => $user->user_birthday,
			'like' => $like
		);
		array_push($resultPostArray, $resultOne);
	}
	$result['user_info'] = $resultPostArray;
	$this->doRespondSuccess($result);
	
}

function getMyPost()
{
	$result = array();
	$resultPostArray = array();

	$condition = array('user_id' => $_POST['user_id']);
    $resultData = $this->api_model->get_values_conditions('tb_post', $condition);
    foreach($resultData as $posts) {

			$resultOne = array(
				'id' => $posts->id,
				'user_id' => $posts->user_id,
				'user_name' => $posts->user_name,
				'user_photo' => $posts->user_photo,
				'thumbnail' => $posts->thumbnail,
				'detailimage' => $posts->detailimage,
				'scale' => $posts->scale
			);
			array_push($resultPostArray, $resultOne);
		}
	$result['post_info'] = $resultPostArray;
	
	$this->db->where('id', $_POST['user_id']);
	$row = $this->db->get('tb_user')->row();
	$result['post_number'] = $row->post_number;
	$result['following'] = $row->following;
	$result['follower'] = $row->follower;
	$result['realname'] = $row->realname;

	$this->doRespondSuccess($result);
	
}

function getOtherUserPost()
{
	$result = array();
	$resultPostArray = array();

	$condition = array('user_id' => $_POST['user_id']);
    $resultData = $this->api_model->get_values_conditions('tb_post', $condition);
    foreach($resultData as $posts) {
            
            $this->db->where('post_id', $posts->id);
			$this->db->where('user_id', $_POST['me_id']);
			$row = $this->db->get('tb_like')->row();
			if (count($row) == 0) {
				$like = '0';
			} else{
				$like = '1';
			}
			
			$resultOne = array(
				'id' => $posts->id,
				'user_id' => $posts->user_id,
				'user_name' => $posts->user_name,
				'user_photo' => $posts->user_photo,
				'thumbnail' => $posts->thumbnail,
				'detailimage' => $posts->detailimage,
				'scale' => $posts->scale,
				'like' => $like
			);
			array_push($resultPostArray, $resultOne);
		}
	$result['post_info'] = $resultPostArray;

	$this->db->where('id', $_POST['user_id']);
	$row = $this->db->get('tb_user')->row();
	$result['post_number'] = $row->post_number;
	$result['following'] = $row->following;
	$result['follower'] = $row->follower;
	$result['realname'] = $row->realname;

	$this->db->where('owner_id', $_POST['user_id']);
	$this->db->where('user_id', $_POST['me_id']);
	$row = $this->db->get('tb_follow')->row();
	if (count($row) == 0) {
		$result['follow_state'] ='0';
	} else{
		$result['follow_state'] ='1';
	}

	$this->doRespondSuccess($result);
	
}

function getOtherUserProfile()
{
	$result = array();
	$this->db->where('id', $_POST['owner_id']);
	$row = $this->db->get('tb_user')->row();
	$result['id'] = $row->id;
	$result['username'] = $row->username;
	$result['picture'] = $row->picture;
	$result['follower'] = $row->follower;
	$result['about_me'] = $row->about_me;
	$result['user_location'] = $row->user_location;
	$result['user_birthday'] = $row->user_birthday;
	
	$this->db->where('user_id', $_POST['user_id']);
	$this->db->where('owner_id', $_POST['owner_id']);
	$row = $this->db->get('tb_follow')->row();
	if (count($row) == 0) {
        $like = '0';
    } else{
        $like = '1';
    }
    $result['like'] = $like;
	$this->doRespondSuccess($result);
}

function like()
{
	$result = array();

	$data = $this->input->post();
	$data['created_at'] = date('Y-m-d H:i:s');
    $this->api_model->insert_query("tb_like", $data);
	
	unset($data['post_id']);
	$data['type'] = 'like';
	$data['read_state'] = 'false';
    $this->api_model->insert_query("tb_notification", $data);
    $this->sendSingleFBPush($data['owner_id'], $data['user_id'].','.$data['user_name'].','.$data['user_photo'], 'like');

	$this->doRespondSuccess($result);
}
function unlike()
{
	$result = array();

	$data = $this->input->post();
	$condition = array('post_id' => $data['post_id'], 'user_id' => $data['user_id']);
    $this->api_model->delete_value("tb_like", $condition);
	
	unset($data['post_id']);
	$data['type'] = 'unlike';
	$data['read_state'] = 'false';
	$data['created_at'] = date('Y-m-d H:i:s');
    $this->api_model->insert_query("tb_notification", $data);
    $this->sendSingleFBPush($data['owner_id'], $data['user_id'].','.$data['user_name'].','.$data['user_photo'], 'unlike');

	$this->doRespondSuccess($result);
}


function follow()// here you must add notification number increasing action for the notification
{
	$result = array();

	$data = $this->input->post();
	$data['created_at'] = date('Y-m-d H:i:s');
    $this->api_model->insert_query("tb_follow", $data);
	
	$data['type'] = 'follow';
	$data['read_state'] = 'false';
    $this->api_model->insert_query("tb_notification", $data);
    $this->sendSingleFBPush($data['owner_id'], $data['user_id'].','.$data['user_name'].','.$data['user_photo'], 'follow');

    $user_id = $data['user_id'];
    $owner_id = $data['owner_id'];

	/// Increasing Follower of Owner
	$this->db->where('id', $owner_id);
	$row = $this->db->get('tb_user')->row();
	$follower = $row->follower;

	$condition = array('id' => $owner_id);
	$data = array('follower' => intval($follower) + 1);
	$this->api_model->update_query("tb_user", $condition, $data);

	$this->doRespondSuccess($result);
}
function unfollow() // here you must decrease the notification number decreasing action for notification
{
	$result = array();

	$data = $this->input->post();
	$condition = array('owner_id' => $data['owner_id'], 'user_id' => $data['user_id']);
    $this->api_model->delete_value("tb_follow", $condition);
	
	$data['type'] = 'unfollow';
	$data['created_at'] = date('Y-m-d H:i:s');
	$data['read_state'] = 'false';
    $this->api_model->insert_query("tb_notification", $data);
    $this->sendSingleFBPush($data['owner_id'], $data['user_id'].','.$data['user_name'].','.$data['user_photo'], 'unfollow');

    $user_id = $data['user_id'];
    $owner_id = $data['owner_id'];
    
	/// Decreasing Follower of Owner
	$this->db->where('id', $owner_id);
	$row = $this->db->get('tb_user')->row();
	$follower = $row->follower;

	$condition = array('id' => $owner_id);
	$data = array('follower' => intval($follower) - 1);
	$this->api_model->update_query("tb_user", $condition, $data);


	$this->doRespondSuccess($result);
}

function getFollower()
{
	$owner_id = $_POST['owner_id'];

	$result = array();

    $condition = array('owner_id' => $owner_id);
    $resultData = $this->api_model->get_values_conditions('tb_follow', $condition);

	$result['follower_info'] = $resultData;
	$this->doRespondSuccess($result);
	
}

function getLikers()// user_id follow owner_id
{
	$user_id = $_POST['user_id'];
	$result = array();
	
	$query = 'SELECT * FROM `tb_user` JOIN tb_follow ON tb_user.id = tb_follow.owner_id WHERE tb_follow.user_id ='.$user_id;
	//echo $query;
	
	$resultData = $this -> db -> query($query) -> result();

	$result['follower_info'] = $resultData;
	$this->doRespondSuccess($result);
	
}

function deletePost()
{
	$post_id = $_POST['post_id'];

	$result = array();

    $condition = array('id' => $post_id);
    $resultData = $this->api_model->delete_value('tb_post', $condition);
	$this->doRespondSuccess($result);
	
}

function closeAccount()
{
	$user_id = $_POST['user_id'];

	$result = array();
    /// delete user in the tb_user
    $condition = array('id' => $user_id);
    $resultData = $this->api_model->delete_value('tb_user', $condition);
    /// delete post in the tb_post
    $conditionPost = array('user_id' => $user_id);
    $resultDataPost = $this->api_model->delete_value('tb_post', $conditionPost);
    /// delete notification in the tb_notification
    $conditionNoti = array('user_id' => $user_id);
    $resultDataNoti = $this->api_model->delete_value('tb_notification', $conditionNoti);
    $conditionNoti1 = array('owner_id' => $user_id);
    $resultDataNoti1 = $this->api_model->delete_value('tb_notification', $conditionNoti1);
    
    /// delete follow in the tb_follow
    $conditionFollow = array('user_id' => $user_id);
    $resultDataFollow = $this->api_model->delete_value('tb_follow', $conditionFollow);
    
    $conditionFollow1 = array('owner_id' => $user_id);
    $resultDataFollow1 = $this->api_model->delete_value('tb_follow', $conditionFollow1);

	$this->doRespondSuccess($result);
}

function getFollowing()
{
	$user_id = $_POST['user_id'];

	$result = array();
	$resultFollowArr = array();

    $condition = array('user_id' => $user_id);
    $resultData = $this->api_model->get_values_conditions('tb_follow', $condition);

    foreach($resultData as $follows) {

    		$this->db->where('id', $follows->owner_id);
			$row = $this->db->get('tb_user')->row();

			$resultOne = array(
				'user_id' => $row->id,
				'user_name' => $row->username,
				'user_photo' => $row->picture
			);
			array_push($resultFollowArr, $resultOne);
		}

	$result['following_info'] = $resultFollowArr;
	$this->doRespondSuccess($result);
}

function getComments()
{
	$post_id = $_POST['post_id'];

	$result = array();
	$resultPostArray = array();

    $condition = array('post_id' => $post_id);
    $resultData = $this->api_model->get_values_conditions('tb_comment', $condition);
	$result['comment_info'] = $resultData;
	$this->doRespondSuccess($result);
	
}

function getNotys()
{
	$owner_id = $_POST['owner_id'];
	$result = array();
	
	$query = 'SELECT * FROM `tb_user` JOIN tb_notification ON tb_user.id = tb_notification.user_id WHERE tb_notification.owner_id = '.$owner_id;
	//echo $query;
	
	$resultData = $this -> db -> query($query) -> result();
	$result['notification_info'] = $resultData;
	$this->doRespondSuccess($result);
	
}


function comment()
{
	$result = array();
	$data = $this->input->post();
	$data['created_at'] = date('Y-m-d H:i:s');
    $this->api_model->insert_query("tb_comment", $data);
	$this->doRespondSuccess($result);
}

function filterusers(){
    $result = array();
    $user_id = $_POST['user_id'];
    $gender= $_POST['gender'];
    $attr = $_POST['attr'];
    $purpose = $_POST['purpose'];
    $age = $_POST['age'];  //10,11
    $nearby = $_POST['nearby']; //0 or 1
    
    /*'
SELECT * FROM tb_user
JOIN tb_purpose ON tb_user.id = tb_purpose.purpose_user_id JOIN tb_attr ON tb_user.id = tb_attr.user_id WHERE (tb_user.user_gender = 'フェム' OR tb_user.user_gender = 'FTM') AND (tb_user.user_birthday >= '2000-01-01' AND tb_user.user_birthday<= '2010-12-31') AND (tb_purpose.purpose = '恋人募集' OR tb_purpose.purpose = 'セクシャル') AND (tb_attr.attribute = 'ネコ' OR tb_attr.attribute = 'ストレート' )';*/
    
    $query = 'SELECT * FROM tb_user';
    if(strlen($purpose)>0) $query .= ' JOIN tb_purpose ON tb_user.id = tb_purpose.purpose_user_id ';
    if(strlen($attr)>0) $query .=' JOIN tb_attr ON tb_user.id = tb_attr.user_id ';
    $query .=' WHERE ';
    
    $sub_query = "tb_user.id != ".$user_id;
    if(strlen($gender)>0){
        $gender_sub_query = "";
        $gender_array = explode(",", $gender);
        foreach($gender_array as $one){
            if(strlen($gender_sub_query)== 0) $gender_sub_query = "tb_user.user_gender ='".$one."'"; 
            else $gender_sub_query .= " OR tb_user.user_gender ='".$one."'"; 
        }
        $sub_query .= " AND (".$gender_sub_query.")";
    }
    
    if(strlen($age)>0){
        $age_sub_query = "";
        $age_array = explode(",", $age);
        $endage = date("Y", strtotime("-".$age_array[0].' year'))."-01-01";
        $startage = date("Y", strtotime("-".$age_array[1].' year'))."-12-31";
        $age_sub_query = "(tb_user.user_birthday >= '".$startage."' AND tb_user.user_birthday<= '".$endage."')";
        if(strlen($sub_query)== 0) $sub_query .= $age_sub_query;
        else $sub_query .= " AND ".$age_sub_query;
    }
    
    if(strlen($purpose)>0){
        $purpose_sub_query = "";
        $purpose_array = explode(",", $purpose);
        foreach($purpose_array as $one){
            if(strlen($purpose_sub_query)== 0) $purpose_sub_query = "tb_purpose.purpose ='".$one."'"; 
            else $purpose_sub_query .= " OR tb_purpose.purpose ='".$one."'"; 
        }
        if(strlen($sub_query)== 0) $sub_query = "(".$purpose_sub_query.")";
        else $sub_query .= " AND "."(".$purpose_sub_query.")";
    }
    
    if(strlen($attr)>0){
        $attr_sub_query = "";
        $attr_array = explode(",", $attr);
        foreach($attr_array as $one){
            if(strlen($attr_sub_query)== 0) $attr_sub_query = "tb_attr.attribute ='".$one."'"; 
            else $attr_sub_query .= " OR tb_attr.attribute ='".$one."'"; 
        }
        if(strlen($sub_query)== 0) $sub_query = "(".$attr_sub_query.")";
        else $sub_query .= " AND "."(".$attr_sub_query.")";
    }
    
    $query.=$sub_query." Group By tb_user.id";
    //echo $query;
    $filtered_users = $this -> db -> query($query) -> result();
    if($nearby == 0) $result['users'] = $filtered_users;
    else{
        $userdata = $this -> db -> where('id', $user_id)-> get('tb_user')-> result();
        $latitude = $userdata[0] -> latitude;
        $longitude = $userdata[0] -> longitude;
        $users = array();
        foreach($filtered_users as $one){
            $user_latitude = $one -> latitude;
            $user_longitude = $one -> longitude;
            
            $distance = $this -> distance($latitude, $longitude, $user_latitude, $user_longitude, "M");
            if($distance <= 5){
                array_push($users, $one);
            }
        }
        $result['users'] = $users;
    }
    
    $this->doRespondSuccess($result);
}


function distance($lat1, $lon1, $lat2, $lon2, $unit) {
  if (($lat1 == $lat2) && ($lon1 == $lon2)) {
    return 0;
  }
  else {
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == "K") {
      return ($miles * 1.609344);
    } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
      return $miles;
    }
  }
}

function getNotification()
{
	$user_id = $_POST['user_id'];

	$result = array();

	$this->db->where('id', $user_id);
	$row = $this->db->get('tb_user')->row();
	$result['state'] = $row->notification;
	$this->doRespondSuccess($result);
	
}
function setNotification()
{

	$result = array();
	$user_id = $_POST['user_id'];
	$state = $_POST['state'];
	$condition = array('id' => $user_id);
	$data = array('notification' => $state);
	$id = $this->api_model->update_query("tb_user", $condition, $data);
	$this->doRespondSuccess($result);
}

function setBlockUser()
{
    $data = $this->input->post();
	$data['created_at'] = date('Y-m-d H:i:s');
    $this->api_model->insert_query("tb_block", $data);
	$result = array();
	$this->doRespondSuccess($result);
}

function getBlockUsers()// user_id follow owner_id
{
	$owner_id = $_POST['owner_id'];
	$result = array();
	
	$query = 'SELECT * FROM `tb_user` JOIN tb_block ON tb_user.id = tb_block.block_user_id WHERE tb_block.owner_id ='.$owner_id;
	//echo $query;
	
	$resultData = $this -> db -> query($query) -> result();

	$result['follower_info'] = $resultData;
	$this->doRespondSuccess($result);
	
}

function removeBlockUser()
{
	$result = array();
	$user_id = $_POST['block_user_id'];
	$owner_id = $_POST['owner_id'];
	
	$condition = array('block_user_id' => $user_id, 'owner_id' => $owner_id);
	$this->api_model->delete_value("tb_block", $condition);
	$this->doRespondSuccess($result);
}



function setNotiRead()
{
	$result = array();
	$owner_id = $_POST['owner_id'];
	$condition = array('owner_id' => $owner_id);
	$data = array('read_state' => "true");
	$id = $this->api_model->update_query("tb_notification", $condition, $data);
	$this->doRespondSuccess($result);
}


function uploadPost() {

	$result = array();
	$data = $this->input->post();
    $data['created_at'] = date('Y-m-d H:i:s');
    $createdtime = $data['created_at'];
	$this->api_model->insert_query('tb_post', $data);
	$this->db->where('created_at', $createdtime);
	$row = $this->db->get('tb_post')->row();
	$result['post_id'] = $row->id;
	$this->doRespondSuccess($result);
	
}

public function makeRandomCode(){

	$random_code = '';

	$arr = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U',
	'V','W','X','Y','Z',/*'1','2','3','4','5','6','7','8','9','0',*/'a','b','c','d','e','f',
	'g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
	$arr = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');

	for ($i = 0 ; $i < 6 ; $i++) {

		$index = mt_rand(0, count($arr) - 1);
		$random_code .= $arr[$index];
	}

	return $random_code;
}
function clean_space($dir) {
    foreach(glob($dir . '/*') as $file) {
        if(is_dir($file))
            $this->clean_space($file);
        else
            unlink($file);
    }
    rmdir($dir);
}
public function sendMail($email, $code){

	$to = $email;
	$subject = "Forgot Password";
	$message = "Pin Code : "
	."<strong><font size = 3>".$code."</font></strong>"."<br><br>" ;
	$message.= "MeetsApp support<br>";
	$message.= '<a href="mailto:support@meets.com" style="color:#104FBE;">support@meets.com</a>';
	 // To send HTML mail, the Content-type header must be set
        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=iso-8859-1';
        
        // Additional headers
        $headers[] = 'From: MeetsApp <support@meets.com>';
        /*$headers[] = 'Cc: birthdayarchive@example.com';
        $headers[] = 'Bcc: birthdaycheck@example.com';*/
        
        // Mail it
        mail($to, $subject, $message, implode("\r\n", $headers));
}

function sendSingleFBPush($user_id, $title, $body) {

	// send FCM push notification
	/*$user_id = $_POST['user_id'];
    $title = $_POST['title'];
    $body = $_POST['body'];*/


	$url = "https://fcm.googleapis.com/fcm/send";

	$token = $this->api_model->getToken($user_id);

	if (strlen($token) == 0 ) {
		return;
	}
	
	$fcmMsg = array(
        'body' 	=> $body,
        'title' => $title,
        'status'=> '0',
        //'sound' => "default",
        //'color' => "#203E78"
    );

	$dataMsg = array
	(
		'body'     => $body,
		'title'    => $title,
		'status'   => '0',
		//'badge' => 1,
		//'sound' => 'default'/*Default sound*/
	);
	$fields = array
	(
		'to'                => $token,
		'data' 		    	=> $dataMsg,
		'notification'      => $fcmMsg,
		'priority'          => 'high',
	);

	$headers = array(
		'Authorization: key=' . $this->FCM_APIKEY,
		'Content-Type: application/json'
	);

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	//curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
	//curl_setopt($ch, CURLOPT_TIMEOUT, 1);

	$result['result'] = curl_exec($ch);

	curl_close($ch);

	return $result;

}

// ***************** special part start ***********************//


public function getMembershipInfo(){
    $user_id = $_POST['user_id'];
	$result = array();
	$this->db->where('user_id', $user_id);
	$row = $this->db->get('tb_user1')->row();
	$result['joined_date'] = $row->joined_date;
	$result['is_plus_member'] = $row->is_plus_member;
	$this->doRespondSuccess($result);
}

public function uploadToken(){
    $user_id = $_POST['user_id'];
    $token = $_POST['token'];
    $joined_date = $_POST['joined_date'];
    $is_plus_member = $_POST['is_plus_member'];
    
    $this->api_model->setToken1($user_id, $token, $joined_date, $is_plus_member);
}

public function chatRequest(){

	$FCM_APIKEY = 'AAAA4FVY1lY:APA91bG1HzJY50YUDcUny3JC6gq1Fer_bDFcytWUm5XtIKliRnS-sjpNNlXh6fPzEIZXPkTZDS7tFLLSVcJpeUDvfxPHoEF7ERiqHt5-rrQ-E_0eTaZX3K4wJ9I6H_0CDhj9h8ytjWuu';

	$data = $this->input->post();
	
	$this->db->where('user_id', $data['receiver_id']);
    $row = $this->db->get('tb_user1')->row();

    if (count($row) == 0) {
      return;
    }
	$url = "https://fcm.googleapis.com/fcm/send";

	$token = $row->token;
	$body = "";
	$title = ""; 
	if ($data['requestType'] == 'chatSend') {
		$body = "Hello,".$data['sender_name']." sent new chat request to you.";
		$title = "New chat request"; 
	} elseif ($data['requestType'] == 'chatAcccept') {
		$body = "Hello,".$data['sender_name']." accepted your request.";
		$title = "Chat request accept"; 
	} elseif ($data['requestType'] == 'chatReject') {
		$body = "Hello,".$data['sender_name']." rejected your request.";
		$title = "Chat request reject"; 
	} elseif ($data['requestType'] == 'verifySend') {
		$body = "Hello,".$data['sender_name']." sent new verify request to you.";
		$title = "New verify request"; 
	} elseif ($data['requestType'] == 'verifyAccept') {
		$body = "Hello,".$data['sender_name']." accepted your verify request.";
		$title = "Verify request accept"; 
	} elseif ($data['requestType'] == 'verifyReject') {
		$body = "Hello,".$data['sender_name']." rejected your verify request.";
		$title = "Verify request reject"; 
	} 

	$fcmMsg = array(
        'body' 	=> $body,
        'title' => $title,
        'status'=> '0',
        'sound' => "default",// here you must set background noti sound by default
        //'color' => "#203E78"
    );

	$dataMsg = array
	(
		'body'     => $body,
		'title'    => $title,
		'status'   => '0',
		//'badge' => 1,
		'sound' => 'default'/*Default sound*/
	);
	$fields = array
	(
		'to'                => $token,
		'data' 		    	=> $dataMsg,
		'notification'      => $fcmMsg,
		'priority'          => 'high',
	);

	$headers = array(
		'Authorization: key=' . $FCM_APIKEY,
		'Content-Type: application/json'
	);

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	//curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
	//curl_setopt($ch, CURLOPT_TIMEOUT, 1);

	$result['result'] = curl_exec($ch);

	curl_close($ch);

	return $result;
}

// ***************** special part end ***********************//

function php_info() {

	phpinfo();
}

}

?>
