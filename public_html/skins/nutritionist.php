<?php $this->load->view('admin/header'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
		<div class='del_res'>
		<?php
		if(isset($succes_message))
    {
    	echo '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <h4><i class="icon fa fa-check"></i> Alert!</h4>'.$succes_message.'
            </div>';
    }	
    if(isset($error_message))
    {
    	echo '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <h4><i class="icon fa fa-check"></i> Alert!</h4>'.$error_message.'
            </div>';
    }
?>
</div>
        <section class="content-header">
          <h1>Nutritionist</h1>
          <ol class="breadcrumb">
            <li>
              <a href="<?php  echo base_url();?>uploadfiles/nutritionist/#">
                <i class="fa fa-dashboard"></i> Home
              </a>
            </li>
            <li class="active">Nutritionist</li>
          </ol>
        </section>

     
		    <!-- Main content -->
        <section class="content">
		      <div class="row">
            <div class="col-md-12">
				      <div class="box">
                <!-- /.box-header -->
                <div class="box-header">
                  <div id="breadcrumb" class="col-md-12" style="background-color:#4d9bc2; margin-bottom: 15px">
                        <h3 class="box-title" style="padding: 10px;">List of Nutritionist</h3>
                    </div>
                  <a class="btn btn-info" href='<?= base_url() ?>admin/add_nutritionist' style="float:right; margin-right:10px;">+Add New Nutritionist</a>
                </div>
                <!-- /.box-body -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped" >
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Sub Title</th>
                        <th>Mini Description</th>
                        <th>Full Description</th>
                        <th>Price</th>
                        <th>Email</th>
                        <th>Name</th>
                        <th style="width:130px; text-align:center;">Settings</th>
                      </tr>
                    </thead>
                    <tbody>
              <?php
                $i = 1;
                foreach($nutritionists as $value) {
              ?>
                    <tr id='<?php echo $value->id; ?>'>
                      <td style="text-align: center;" data-id="<?php echo $value->id; ?>"><?php echo $i ?></td>
                      <td><img width="130px" height="90px" src=<?php echo $value->image;?> /></td>
                      <td><?php echo $value->title ?></td>
                      <td><?php echo $value->subtitle ?></td>
                      <td><?php echo $value->minidescription ?></td>
                      <td><?php echo $value->fulldescription ?></td>
                      <td><?php echo $value->price ?></td>
                      <td><?php echo $value->email ?></td>
                      <td><?php echo $value->name ?></td>
                      <td style="text-align: center;"><a class="btn btn-warning" href='<?= base_url() ?>admin/edit_nutritionist/<?= $value->id ?>' style="float:center; margin-bottom:10px;">Edit</a>
                      <a class="btn btn-danger" onclick="deleteEntity(<?= $value->id ?>)" style="float:center; margin-bottom:10px;">Delete</a></td>
                    </tr>
              <?php $i++; }?>                    
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	 
   <script>
    function deleteEntity(id){
          var r;
          r = confirm("Are you sure to delete this Nutrition Product?");

          if (r == true) {
             location.href = "<?php echo base_url();?>"+"admin/delete_vnc/tb_nutritionists/nutritionist/" + id;
          }
      }
   </script>
  <?php $this->load->view('admin/footer'); ?>
  
 