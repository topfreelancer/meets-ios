-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 23, 2020 at 02:24 AM
-- Server version: 5.6.49-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meets_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_user1`
--

CREATE TABLE `tb_user1` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `joined_date` varchar(255) NOT NULL,
  `is_plus_member` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user1`
--

INSERT INTO `tb_user1` (`id`, `user_id`, `token`, `joined_date`, `is_plus_member`) VALUES
(18, '5f22d9cdd2c7032c76cf9457', 'fFaYaK2n70fbnoWTGrcwhf:APA91bFeKn581JLRXhV8zzCN9Wny3sm2AbR5YvTN3PGuX0kwAwkbY-o-zESCyNaQKWW8eEzdxvcj8l4TY2EVbSBReBJ9jpQxwwhGHsxHeX6qbCTQO2uV8TnVmjhLx6YsS1u9smvKD9Bf', '1605877193000', 'false'),
(17, '5f2ad841b573b835e52201c7', 'dLV05SON7kVHt8Kaqt0tdW:APA91bGoEAf5_nE6IgauFiTBs-mFfONZq5ytsvWRYGrf31UV7yHuy87BoSp8320m9lBW-czU-_-Oq41Nwl-QIBTExkAlujlKlxAnKQipjNJZ_fmzzTfLsjM8Osxka_XLX3dLFtKZS5Rt', '1605877174000', 'false');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_user1`
--
ALTER TABLE `tb_user1`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_user1`
--
ALTER TABLE `tb_user1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
