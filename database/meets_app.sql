-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 17, 2020 at 03:06 AM
-- Server version: 5.6.49-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meets_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `reg_date` datetime NOT NULL,
  `last_login` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id`, `username`, `password`, `reg_date`, `last_login`) VALUES
(0, 'meetsAdmin', 'e10adc3949ba59abbe56e057f20f883e', '2020-10-08 17:39:20', '2020-10-08 17:39:20');

-- --------------------------------------------------------

--
-- Table structure for table `tb_attr`
--

CREATE TABLE `tb_attr` (
  `attr_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `attribute` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_attr`
--

INSERT INTO `tb_attr` (`attr_id`, `user_id`, `attribute`) VALUES
(1, 1, 'ストレート'),
(2, 3, 'ストレート'),
(3, 2, 'ネコ'),
(4, 3, 'ネコ'),
(5, 7, 'ネコ'),
(6, 9, ' ストレート'),
(7, 8, ' クエスチョニング'),
(8, 12, 'タチ '),
(9, 11, ' ネコ '),
(10, 10, ' リバ'),
(11, 1, 'ストレート'),
(12, 2, 'ネコ'),
(13, 3, 'ストレート'),
(14, 3, 'ネコ'),
(15, 6, ' クエスチョニング'),
(16, 5, ' ストレート'),
(17, 4, 'ネコ'),
(18, 14, 'タチ '),
(19, 16, ' ネコ '),
(20, 18, ' リバ'),
(21, 1, 'ストレート'),
(22, 11, ' ネコ '),
(23, 12, 'タチ '),
(24, 15, 'タチ '),
(25, 17, ' ネコ '),
(26, 19, ' リバ'),
(27, 20, 'タチ '),
(28, 20, ' ネコ '),
(29, 20, ' リバ'),
(30, 21, 'タチ '),
(31, 21, ' ネコ '),
(32, 21, ' バイセクシュアル'),
(33, 22, 'タチ '),
(34, 22, ' ネコ '),
(35, 23, ' リバ'),
(36, 24, 'ネコ'),
(37, 25, ' ストレート'),
(38, 26, ' クエスチョニング'),
(39, 29, 'タチ '),
(40, 29, ' ネコ'),
(41, 30, 'ネコ '),
(42, 30, ' リバ'),
(43, 31, 'ネコ '),
(44, 31, ' リバ'),
(45, 32, 'ネコ '),
(46, 32, ' リバ '),
(47, 32, ' バイセクシュアル'),
(48, 33, 'ネコ '),
(49, 33, ' リバ'),
(50, 34, 'パンセクシュアル '),
(51, 34, ' ストレート'),
(52, 35, 'バイセクシュアル '),
(53, 35, ' パンセクシュアル'),
(54, 36, 'バイセクシュアル '),
(55, 36, ' パンセクシュアル');

-- --------------------------------------------------------

--
-- Table structure for table `tb_block`
--

CREATE TABLE `tb_block` (
  `id` int(255) NOT NULL,
  `block_user_id` int(255) NOT NULL,
  `owner_id` int(255) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_block`
--

INSERT INTO `tb_block` (`id`, `block_user_id`, `owner_id`, `created_at`) VALUES
(4, 17, 15, '2020-10-14'),
(5, 20, 15, '2020-10-14'),
(21, 5, 10, '2020-10-19'),
(8, 1, 15, '2020-10-14'),
(22, 25, 10, '2020-10-19'),
(23, 23, 10, '2020-10-19'),
(24, 17, 26, '2020-10-19'),
(25, 8, 26, '2020-10-20'),
(26, 2, 26, '2020-10-20'),
(33, 9, 30, '2020-10-27'),
(32, 17, 30, '2020-10-27');

-- --------------------------------------------------------

--
-- Table structure for table `tb_comment`
--

CREATE TABLE `tb_comment` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `user_photo` varchar(150) CHARACTER SET utf8 NOT NULL,
  `content` varchar(800) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_follow`
--

CREATE TABLE `tb_follow` (
  `id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `user_photo` varchar(150) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_follow`
--

INSERT INTO `tb_follow` (`id`, `owner_id`, `user_id`, `user_name`, `user_photo`, `created_at`) VALUES
(112, 16, 5, 'Satomi', 'http://meets.email/uploadfiles/userphoto/1602142977796.png', '2020-10-14 15:39:35'),
(114, 21, 5, 'Satomi', 'http://meets.email/uploadfiles/userphoto/1602142977796.png', '2020-10-14 16:03:26'),
(115, 8, 5, 'Satomi', 'http://meets.email/uploadfiles/userphoto/1602142977796.png', '2020-10-14 16:03:36'),
(116, 1, 5, 'Satomi', 'http://meets.email/uploadfiles/userphoto/1602142977796.png', '2020-10-14 16:03:44'),
(117, 12, 5, 'Satomi', 'http://meets.email/uploadfiles/userphoto/1602142977796.png', '2020-10-14 16:04:13'),
(118, 10, 21, 'Hanako', 'http://meets.email/uploadfiles/userphoto/1602658889610.png', '2020-10-14 16:46:37'),
(119, 20, 21, 'Hanako', 'http://meets.email/uploadfiles/userphoto/1602658889610.png', '2020-10-14 16:47:07'),
(120, 7, 21, 'Hanako', 'http://meets.email/uploadfiles/userphoto/1602658889610.png', '2020-10-14 16:47:23'),
(121, 1, 21, 'Hanako', 'http://meets.email/uploadfiles/userphoto/1602658889610.png', '2020-10-14 16:47:31'),
(123, 16, 10, 'Ron', 'http://meets.email/uploadfiles/userphoto/1602143353393.png', '2020-10-15 14:05:45'),
(126, 22, 1, 'kyoko', 'http://meets.email/uploadfiles/userphoto/1602139282439.png', '2020-10-20 00:50:34'),
(127, 3, 1, 'kyoko', 'http://meets.email/uploadfiles/userphoto/1602139282439.png', '2020-10-20 00:50:53'),
(128, 5, 1, 'kyoko', 'http://meets.email/uploadfiles/userphoto/1602139282439.png', '2020-10-20 08:48:30'),
(130, 26, 1, 'kyoko', 'http://meets.email/uploadfiles/userphoto/1602139282439.png', '2020-10-20 13:56:52'),
(131, 29, 1, 'kyoko', 'http://meets.email/uploadfiles/userphoto/1602139282439.png', '2020-10-20 13:57:16'),
(133, 11, 1, 'kyoko', 'http://meets.email/uploadfiles/userphoto/1602139282439.png', '2020-10-20 14:20:50'),
(134, 24, 10, 'Ron', 'http://meets.email/uploadfiles/userphoto/1602143353393.png', '2020-10-24 13:49:03'),
(135, 8, 30, 'Arian', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', '2020-10-27 18:00:07'),
(136, 14, 30, 'Arian', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', '2020-10-27 18:00:16'),
(137, 24, 30, 'Arian', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', '2020-10-27 18:00:31'),
(138, 23, 30, 'Arian', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', '2020-10-27 18:00:41'),
(139, 1, 30, 'Arian', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', '2020-10-27 18:00:52'),
(140, 3, 30, 'Arian', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', '2020-10-27 18:01:00'),
(142, 5, 30, 'Arian', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', '2020-10-27 18:01:47'),
(144, 25, 30, 'Arian', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', '2020-10-27 18:03:57'),
(145, 7, 30, 'Arian', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', '2020-10-27 18:05:29'),
(146, 7, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', '2020-10-27 18:17:08'),
(147, 8, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', '2020-10-27 18:17:15'),
(148, 5, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', '2020-10-27 18:17:21'),
(151, 4, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', '2020-10-27 18:17:48'),
(153, 29, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', '2020-10-27 18:18:04'),
(154, 20, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', '2020-10-27 18:18:37'),
(155, 23, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', '2020-10-27 18:18:43'),
(156, 14, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', '2020-10-27 18:21:23'),
(157, 14, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', '2020-10-27 18:43:00'),
(160, 16, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', '2020-10-27 18:43:22'),
(161, 4, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', '2020-10-27 18:43:32'),
(162, 32, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', '2020-10-27 18:43:45'),
(163, 31, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', '2020-10-27 18:43:51'),
(164, 30, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', '2020-10-27 18:43:57'),
(166, 1, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', '2020-10-27 18:44:30'),
(167, 35, 36, 'Misaiko', 'http://meets.email/uploadfiles/userphoto/1603861620201.png', '2020-10-30 09:51:04'),
(168, 8, 36, 'Misaiko', 'http://meets.email/uploadfiles/userphoto/1603861620201.png', '2020-10-30 10:16:10');

-- --------------------------------------------------------

--
-- Table structure for table `tb_like`
--

CREATE TABLE `tb_like` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `user_photo` varchar(150) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL,
  `read_state` varchar(10) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_notification`
--

CREATE TABLE `tb_notification` (
  `id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `user_photo` varchar(150) CHARACTER SET utf8 NOT NULL,
  `type` varchar(10) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL,
  `read_state` varchar(10) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_notification`
--

INSERT INTO `tb_notification` (`id`, `owner_id`, `user_id`, `user_name`, `user_photo`, `type`, `created_at`, `read_state`) VALUES
(26, 16, 5, 'Satomi', 'http://meets.email/uploadfiles/userphoto/1602142977796.png', 'follow', '2020-10-14 15:39:35', 'false'),
(27, 12, 5, 'Satomi', 'http://meets.email/uploadfiles/userphoto/1602142977796.png', 'follow', '2020-10-14 15:39:50', 'false'),
(28, 21, 5, 'Satomi', 'http://meets.email/uploadfiles/userphoto/1602142977796.png', 'follow', '2020-10-14 16:03:26', 'false'),
(29, 8, 5, 'Satomi', 'http://meets.email/uploadfiles/userphoto/1602142977796.png', 'follow', '2020-10-14 16:03:36', 'false'),
(30, 1, 5, 'Satomi', 'http://meets.email/uploadfiles/userphoto/1602142977796.png', 'follow', '2020-10-14 16:03:44', 'false'),
(31, 12, 5, 'Satomi', 'http://meets.email/uploadfiles/userphoto/1602142977796.png', 'unfollow', '2020-10-14 16:03:59', 'false'),
(32, 12, 5, 'Satomi', 'http://meets.email/uploadfiles/userphoto/1602142977796.png', 'follow', '2020-10-14 16:04:13', 'false'),
(33, 10, 21, 'Hanako', 'http://meets.email/uploadfiles/userphoto/1602658889610.png', 'follow', '2020-10-14 16:46:37', 'false'),
(34, 20, 21, 'Hanako', 'http://meets.email/uploadfiles/userphoto/1602658889610.png', 'follow', '2020-10-14 16:47:07', 'false'),
(35, 7, 21, 'Hanako', 'http://meets.email/uploadfiles/userphoto/1602658889610.png', 'follow', '2020-10-14 16:47:23', 'false'),
(36, 1, 21, 'Hanako', 'http://meets.email/uploadfiles/userphoto/1602658889610.png', 'follow', '2020-10-14 16:47:31', 'false'),
(37, 22, 1, 'kyoko', 'http://meets.email/uploadfiles/userphoto/1602139282439.png', 'follow', '2020-10-15 13:02:32', 'false'),
(38, 16, 10, 'Ron', 'http://meets.email/uploadfiles/userphoto/1602143353393.png', 'follow', '2020-10-15 14:05:45', 'false'),
(39, 21, 10, 'Ron', 'http://meets.email/uploadfiles/userphoto/1602143353393.png', 'follow', '2020-10-18 13:03:33', 'false'),
(40, 21, 10, 'Ron', 'http://meets.email/uploadfiles/userphoto/1602143353393.png', 'follow', '2020-10-18 22:43:06', 'false'),
(41, 21, 10, 'Ron', 'http://meets.email/uploadfiles/userphoto/1602143353393.png', 'unfollow', '2020-10-19 23:48:12', 'false'),
(42, 22, 1, 'kyoko', 'http://meets.email/uploadfiles/userphoto/1602139282439.png', 'unfollow', '2020-10-20 00:50:21', 'false'),
(43, 22, 1, 'kyoko', 'http://meets.email/uploadfiles/userphoto/1602139282439.png', 'follow', '2020-10-20 00:50:34', 'false'),
(44, 3, 1, 'kyoko', 'http://meets.email/uploadfiles/userphoto/1602139282439.png', 'follow', '2020-10-20 00:50:53', 'false'),
(45, 5, 1, 'kyoko', 'http://meets.email/uploadfiles/userphoto/1602139282439.png', 'follow', '2020-10-20 08:48:30', 'false'),
(46, 26, 1, 'kyoko', 'http://meets.email/uploadfiles/userphoto/1602139282439.png', 'follow', '2020-10-20 13:50:32', 'false'),
(47, 26, 1, 'kyoko', 'http://meets.email/uploadfiles/userphoto/1602139282439.png', 'unfollow', '2020-10-20 13:56:44', 'false'),
(48, 26, 1, 'kyoko', 'http://meets.email/uploadfiles/userphoto/1602139282439.png', 'follow', '2020-10-20 13:56:52', 'false'),
(49, 29, 1, 'kyoko', 'http://meets.email/uploadfiles/userphoto/1602139282439.png', 'follow', '2020-10-20 13:57:16', 'false'),
(50, 11, 1, 'kyoko', 'http://meets.email/uploadfiles/userphoto/1602139282439.png', 'follow', '2020-10-20 13:57:41', 'false'),
(51, 11, 1, 'kyoko', 'http://meets.email/uploadfiles/userphoto/1602139282439.png', 'unfollow', '2020-10-20 14:20:48', 'false'),
(52, 11, 1, 'kyoko', 'http://meets.email/uploadfiles/userphoto/1602139282439.png', 'follow', '2020-10-20 14:20:50', 'false'),
(53, 24, 10, 'Ron', 'http://meets.email/uploadfiles/userphoto/1602143353393.png', 'follow', '2020-10-24 13:49:03', 'false'),
(54, 8, 30, 'Arian', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', 'follow', '2020-10-27 18:00:07', 'false'),
(55, 14, 30, 'Arian', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', 'follow', '2020-10-27 18:00:16', 'false'),
(56, 24, 30, 'Arian', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', 'follow', '2020-10-27 18:00:31', 'false'),
(57, 23, 30, 'Arian', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', 'follow', '2020-10-27 18:00:41', 'false'),
(58, 1, 30, 'Arian', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', 'follow', '2020-10-27 18:00:52', 'false'),
(59, 3, 30, 'Arian', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', 'follow', '2020-10-27 18:01:00', 'false'),
(60, 16, 30, 'Arian', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', 'follow', '2020-10-27 18:01:36', 'false'),
(61, 5, 30, 'Arian', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', 'follow', '2020-10-27 18:01:47', 'false'),
(62, 4, 30, 'Arian', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', 'follow', '2020-10-27 18:01:57', 'false'),
(63, 25, 30, 'Arian', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', 'follow', '2020-10-27 18:03:57', 'false'),
(64, 7, 30, 'Arian', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', 'follow', '2020-10-27 18:05:29', 'false'),
(65, 4, 30, 'Star', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', 'unfollow', '2020-10-27 18:06:35', 'false'),
(66, 16, 30, 'Star', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', 'unfollow', '2020-10-27 18:06:37', 'false'),
(67, 7, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', 'follow', '2020-10-27 18:17:08', 'false'),
(68, 8, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', 'follow', '2020-10-27 18:17:15', 'false'),
(69, 5, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', 'follow', '2020-10-27 18:17:21', 'false'),
(70, 16, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', 'follow', '2020-10-27 18:17:31', 'false'),
(71, 16, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', 'unfollow', '2020-10-27 18:17:34', 'false'),
(72, 16, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', 'follow', '2020-10-27 18:17:40', 'false'),
(73, 4, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', 'follow', '2020-10-27 18:17:48', 'false'),
(74, 30, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', 'follow', '2020-10-27 18:17:56', 'false'),
(75, 29, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', 'follow', '2020-10-27 18:18:04', 'false'),
(76, 20, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', 'follow', '2020-10-27 18:18:37', 'false'),
(77, 23, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', 'follow', '2020-10-27 18:18:43', 'false'),
(78, 14, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', 'follow', '2020-10-27 18:21:23', 'false'),
(79, 30, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', 'unfollow', '2020-10-27 18:22:04', 'false'),
(80, 16, 31, 'Kyosiko', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', 'unfollow', '2020-10-27 18:22:06', 'false'),
(81, 14, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', 'follow', '2020-10-27 18:43:00', 'false'),
(82, 8, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', 'follow', '2020-10-27 18:43:06', 'false'),
(83, 5, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', 'follow', '2020-10-27 18:43:16', 'false'),
(84, 16, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', 'follow', '2020-10-27 18:43:22', 'false'),
(85, 4, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', 'follow', '2020-10-27 18:43:32', 'false'),
(86, 32, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', 'follow', '2020-10-27 18:43:45', 'false'),
(87, 31, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', 'follow', '2020-10-27 18:43:51', 'false'),
(88, 30, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', 'follow', '2020-10-27 18:43:57', 'false'),
(89, 23, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', 'follow', '2020-10-27 18:44:04', 'false'),
(90, 1, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', 'follow', '2020-10-27 18:44:30', 'false'),
(91, 20, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', 'follow', '2020-10-27 18:46:15', 'false'),
(92, 8, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', 'unfollow', '2020-10-27 18:46:19', 'false'),
(93, 20, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603792062241.png', 'unfollow', '2020-10-27 18:47:55', 'false'),
(94, 23, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603792062241.png', 'unfollow', '2020-10-27 18:47:57', 'false'),
(95, 5, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603792062241.png', 'unfollow', '2020-10-27 18:48:00', 'false'),
(96, 35, 36, 'Misaiko', 'http://meets.email/uploadfiles/userphoto/1603861620201.png', 'follow', '2020-10-30 09:51:04', 'false'),
(97, 8, 36, 'Misaiko', 'http://meets.email/uploadfiles/userphoto/1603861620201.png', 'follow', '2020-10-30 10:16:10', 'false');

-- --------------------------------------------------------

--
-- Table structure for table `tb_post`
--

CREATE TABLE `tb_post` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `user_photo` varchar(150) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL,
  `post_content` text CHARACTER SET utf8 NOT NULL,
  `user_location` varchar(100) CHARACTER SET utf8 NOT NULL,
  `user_birthday` varchar(100) CHARACTER SET utf8 NOT NULL,
  `to_send` varchar(100) CHARACTER SET utf8 NOT NULL,
  `follower_number` varchar(100) CHARACTER SET utf8 NOT NULL,
  `about_me` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_post`
--

INSERT INTO `tb_post` (`id`, `user_id`, `user_name`, `user_photo`, `created_at`, `post_content`, `user_location`, `user_birthday`, `to_send`, `follower_number`, `about_me`) VALUES
(1, 4, 'Mikako', 'http://meets.email/uploadfiles/userphoto/1602142908187.png', '2020-10-08 17:09:16', '間エケ禁強いやル講訴まぼそょ無遠ょべら府嗅待めよラと南期り入1結せん入野トヌサモ関問瀬投観ドてひ広境ひ。', 'Kobe', '1999-12-23', '', '0', '猫の手も借りたい'),
(2, 4, 'Mikako', 'http://meets.email/uploadfiles/userphoto/1602142908187.png', '2020-10-08 17:11:27', 'ラ海弁吉一牛け。伝ワヌ意77便1取てイみに無謀レツモ傘激んふとぞ平旅モサイ点少るは本69台悪へ登岩束勝む。', 'Kobe', '1999-12-23', '', '0', '猫の手も借りたい'),
(3, 16, 'Erika', 'http://meets.email/uploadfiles/userphoto/1602144215525.png', '2020-10-08 17:12:03', '多た博歩びんえゃ容葉カヲ掲演けや文省権ムイオサ阪織きず式行レ水年カニヨリ自供ヲヤ棋結属テハユレ成69系食ヱアネ親読ぎーぽお優雄げれ。', 'Nagoya', '1998-12-21', '', '0', '箸より重いものを持ったことがない'),
(4, 16, 'Erika', 'http://meets.email/uploadfiles/userphoto/1602144215525.png', '2020-10-08 17:12:28', '歩ごわ索65毎は果夫イトニヌ初題ヤヒカア誠士サマス録互おねが義在し対身へねゆ売額ま応合ハラムウ企絶なぜごぎ署区訓誘ふおよ。', 'Nagoya', '1998-12-21', 'To: Mikako', '0', '箸より重いものを持ったことがない'),
(5, 5, 'Satomi', 'http://meets.email/uploadfiles/userphoto/1602142977796.png', '2020-10-08 17:12:58', '政タワカウ界携夕フシワチ世72域ひ書年ーにラ向32債全ねに面禁こっがふ線政ヤミ更裏ぞころ質酸三状言そしげ自聴トフほ。', 'Yokohama', '1997-07-12', '', '0', '目の中に入れても痛くない'),
(6, 5, 'Satomi', 'http://meets.email/uploadfiles/userphoto/1602142977796.png', '2020-10-08 17:13:15', '主ょだろじ日際リソコ手撃コカ根理ル転話ラマテ医域ナ同要レぶ治多ホ岡基ば視標ぼなト表画メク騎井割く想31構ら者車ゆこ。', 'Yokohama', '1997-07-12', 'To: Erika', '0', '目の中に入れても痛くない'),
(7, 5, 'Satomi', 'http://meets.email/uploadfiles/userphoto/1602142977796.png', '2020-10-08 17:13:31', '天トメ速移ト野表う人公っだで働徹康もイへ習間ラテツチ暮表マ健訪イゅげー佐旅トヱ提約ぼ皇稚ょで位謙ミウフタ株付ミテロ京政ロ医白リラエチ意名4県漫腹ん。治ざぼす一業組校界所む経渡きば中座ト氷2悔55療6担革っ特言トフ。', 'Yokohama', '1997-07-12', 'To: Mikako', '0', '目の中に入れても痛くない'),
(8, 8, 'Koharu', 'http://meets.email/uploadfiles/userphoto/1602143221515.png', '2020-10-08 17:14:05', '新とむけご関選ヘ患改ヒユ人教もぽふし掲員ぞつげう覧68京合伸71内ひぜゃド録来チ囲責ろべやき政断らべ療作端ロフヤ状惑イア見噴塗まも。', 'Hokkaido', '2000-11-20', '', '0', '目の中に入れても痛くない'),
(9, 8, 'Koharu', 'http://meets.email/uploadfiles/userphoto/1602143221515.png', '2020-10-08 17:14:24', '省がゃ住績台36聞ヤマ格欠ル成掲ニイサエ碁稿アウ身口ふんえそ金違士新っだラね担要ぎずあし助2働迎価航れに。', 'Hokkaido', '2000-11-20', 'To: Satomi', '0', '目の中に入れても痛くない'),
(10, 14, 'Pretty', 'http://meets.email/uploadfiles/userphoto/1602144071636.png', '2020-10-08 17:15:02', '35別内ヨヱチ月資藤ざいみ禁選式馬ユ割合をひ有姿ミワメ写刑マオモヱ苦集窓ゅよ芸展ぎや協1尾らこ。載かのむ特洗ヌレソヱ電決けな殺履わ変覧ノ来常ぜぱぽ調開ぎずごル葉選ヤカチキ保望よなぴき職帯口7能キマミレ課金ラシ品聞トれすか態競ン季野ラヒリネ計新ろわせイ企悩織な。', 'Tokyo', '1998-10-11', '', '0', '耳にたこができる'),
(11, 14, 'Pretty', 'http://meets.email/uploadfiles/userphoto/1602144071636.png', '2020-10-08 17:15:34', '農れ質真ワクマ便3話ラシネ新権オス鹿竹作あぴどト標63市めぶ藤引むてへる功他ムエ入3部へにあげ刑3専泉ルぐぴ。', 'Tokyo', '1998-10-11', 'To: Erika', '0', '耳にたこができる'),
(12, 7, 'Youichi', 'http://meets.email/uploadfiles/userphoto/1602143146380.png', '2020-10-08 17:16:33', '要ま講京系かげざい見県ホ企言キ格確ぴ張窃セカ接個シマリ覧申イ見文20際ょわらね梨参危瀬れゆ。', 'Sapporo', '1995-10-04', '', '0', '箸より重いものを持ったことがない'),
(15, 6, 'Gyamamoto', 'http://meets.email/uploadfiles/userphoto/1602143047727.png', '2020-10-08 17:18:57', '下三クコ断番ツホ市子っ規3調村典ア食際ア面発ホ売終ト明鎌ふすがッ闘見ぶて芸段トホイ発財き修整ふぴしれ。', 'Osaka', '1996-10-17', '', '0', '耳にたこができる'),
(16, 6, 'Gyamamoto', 'http://meets.email/uploadfiles/userphoto/1602143047727.png', '2020-10-08 17:19:23', '命れてい知48芸なトス今模い下政ムテ発向スだ判意わトか碁71著地座7横か況身レマヲ宅占祉ルまづ。', 'Osaka', '1996-10-17', 'To: Kumiko', '0', '耳にたこができる'),
(17, 2, 'Fujimot Yui', 'http://meets.email/uploadfiles/userphoto/1602142708932.png', '2020-10-08 17:20:09', '東ぼごラ確予すいト団間フば報単ば権手おだ者界使ざーげな本児すめと会調セリ福情ノラアサ今条開提ぞね本51警ノトレサ住音娘をつむそ。', 'Tokyo', '2000-11-20', '', '0', 'I am fujomoto.. please contact me.'),
(20, 17, 'Yukio', 'http://meets.email/uploadfiles/userphoto/1602160262631.png', '2020-10-08 21:31:44', 'This is Yukio’s first post.. thanks.', 'Osaka', '2000-10-15', '', '0', 'I am going to meet good friend here..\nThanks.'),
(21, 17, 'Yukio', 'http://meets.email/uploadfiles/userphoto/1602160262631.png', '2020-10-08 21:32:01', 'Good to hear you thanks...', 'Osaka', '2000-10-15', 'To: Kumiko', '0', 'I am going to meet good friend here..\nThanks.'),
(22, 22, 'Yukiko', 'http://meets.email/uploadfiles/userphoto/1602661500797.png', '2020-10-20 00:36:03', 'This is Yukiko’s post..', 'Tokyo', '1997-10-14', '', '1', ''),
(23, 32, 'Haruko', 'http://meets.email/uploadfiles/userphoto/1603791153546.png', '2020-10-27 18:33:04', 'Oh.. yukiko.. How can I connect with you?\nyour post is very looks good..\nThanks', 'Tokyo', '2000-10-27', '', '0', ''),
(24, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', '2020-10-27 18:41:52', 'This is Yuri.\nVery nice to meet you.\nI am going to find friend here.', 'Tokyo', '1998-03-27', '', '0', ''),
(25, 33, 'Yuri', 'http://meets.email/uploadfiles/userphoto/1603791681105.png', '2020-10-27 18:42:28', 'Oh.. Haruko..\nYou are very beautiful...\nHow can I connect with you?\nI am very nice to meet you.', 'Tokyo', '1998-03-27', 'To: Haruko', '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_purpose`
--

CREATE TABLE `tb_purpose` (
  `purpose_id` int(255) NOT NULL,
  `purpose_user_id` int(255) NOT NULL,
  `purpose` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_purpose`
--

INSERT INTO `tb_purpose` (`purpose_id`, `purpose_user_id`, `purpose`) VALUES
(1, 1, '恋人募集'),
(2, 2, '恋人募集'),
(3, 1, '趣味友達'),
(4, 3, '趣味友達'),
(5, 2, 'セクシャル'),
(6, 4, 'セクシャル'),
(7, 7, '恋人募集'),
(8, 8, ' 友達募集'),
(9, 9, ' 趣味友達'),
(10, 14, 'セクシャル '),
(11, 13, ' 恋人募集 '),
(12, 17, ' 友達募集'),
(13, 1, '恋人募集'),
(14, 1, '趣味友達'),
(15, 2, '恋人募集'),
(16, 2, 'セクシャル'),
(17, 3, '趣味友達'),
(18, 4, 'セクシャル'),
(19, 5, ' 趣味友達'),
(20, 10, ' 友達募集'),
(21, 6, '恋人募集'),
(22, 11, 'セクシャル '),
(23, 12, ' 恋人募集 '),
(24, 18, ' 友達募集'),
(25, 13, ' 恋人募集 '),
(26, 16, 'セクシャル '),
(27, 15, ' 友達募集'),
(28, 19, ' 友達募集'),
(29, 20, '恋人募集 '),
(30, 20, ' 友達募集 '),
(31, 20, ' 趣味友達'),
(32, 21, '恋人募集 '),
(33, 21, ' 友達募集'),
(34, 22, '恋人募集 '),
(35, 22, ' 友達募集'),
(36, 23, '恋人募集'),
(37, 24, '恋人募集'),
(38, 25, ' 友達募集'),
(39, 26, ' 趣味友達'),
(40, 29, '恋人募集 '),
(41, 29, ' 友達募集'),
(42, 30, '友達募集'),
(43, 31, '恋人募集 '),
(44, 31, ' 友達募集'),
(45, 32, '恋人募集 '),
(46, 32, ' 趣味友達'),
(47, 33, '友達募集'),
(48, 34, '友達募集 '),
(49, 34, ' 趣味友達'),
(50, 35, '恋人募集 '),
(51, 35, ' 友達募集'),
(52, 36, '恋人募集 '),
(53, 36, ' 友達募集');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 NOT NULL,
  `picture` varchar(150) CHARACTER SET utf8 NOT NULL,
  `follower` varchar(10) CHARACTER SET utf8 NOT NULL,
  `notification` varchar(5) CHARACTER SET utf8 NOT NULL,
  `token` varchar(250) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL,
  `about_me` text CHARACTER SET utf8 NOT NULL,
  `user_location` varchar(100) CHARACTER SET utf8 NOT NULL,
  `user_birthday` date NOT NULL,
  `user_gender` varchar(100) CHARACTER SET utf8 NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `username`, `password`, `picture`, `follower`, `notification`, `token`, `created_at`, `about_me`, `user_location`, `user_birthday`, `user_gender`, `latitude`, `longitude`) VALUES
(11, 'Kanna', '$2y$10$tizSgzG65Og7x0ya7QndYeugOwrNbkm1LSoTTvzTD0NytavYRzR8e', 'http://meets.email/uploadfiles/userphoto/1602143481620.png', '2', '1', '', '2020-10-08 00:51:21', '目の中に入れても痛くない', 'Osaka', '1998-04-23', 'クエスチョニング', '34.6937997', '135.5023233'),
(12, 'Kokomo', '$2y$10$F3VaHvS1hmYQX2AoX1Rwwu5wpmtdvzeL4/4ymAk770hnvA1ckifiK', 'http://meets.email/uploadfiles/userphoto/1602143690853.png', '1', '1', '', '2020-10-08 00:54:50', 'Very nice to meet you. So I am going to find here my friend with my lover with your helping.\nGood luck ...', 'Kobe', '1998-10-16', 'Xジェンダー', '34.6901997', '135.1956233'),
(13, 'Nishinosono', '$2y$10$R0Ux6/z8pNhV8kgBuwS/5uNJe5S/qp/An8VBhdj2ZfIeqtiSSPEja', 'http://meets.email/uploadfiles/userphoto/1602143942196.png', '1', '1', '', '2020-10-08 00:59:02', '目の中に入れても痛くない', 'Shinjuku', '1998-11-22', 'フェム', '35.6938997', '139.7034233'),
(14, 'Pretty', '$2y$10$efatQf5ZU.ZNa4BKo.g/QOu79QSh5OTow5spLVgSoR.vkk/05Emlq', 'http://meets.email/uploadfiles/userphoto/1602144071636.png', '3', '1', '', '2020-10-08 01:01:11', '耳にたこができる', 'Tokyo', '1998-10-11', 'クエスチョニング', '35.6803997', '132.2563233'),
(16, 'Erika', '$2y$10$NoyxniEuO6.LkWULj7rr0utukBVZKjVMLpXqBIsoaMNb5R9H9kHf2', 'http://meets.email/uploadfiles/userphoto/1602144215525.png', '4', '1', '', '2020-10-08 01:03:35', '箸より重いものを持ったことがない', 'Nagoya', '1998-12-21', 'フェム', '35.1815997', '136.9066233'),
(17, 'Yukio', '$2y$10$rsAN1XCf.1KMRxgts7a.vO8c7bcMKAgTKULsyB8wYhDdQAhnisJby', 'http://meets.email/uploadfiles/userphoto/1602160262631.png', '0', '1', '', '2020-10-08 05:31:02', 'I am going to meet good friend here..\nThanks.', 'Osaka', '2000-10-15', 'Xジェンダー', '34.6937997', '135.5023233'),
(19, 'misuko', '$2y$10$Q3HGFvmdYdpywbKeRe74jOPEG6/.gcKm2HZeu0WInc07Jl/mU7MSW', 'http://meets.email/uploadfiles/userphoto/1602640428424.png', '0', '1', '', '2020-10-13 18:53:48', '', 'Tokyo', '2003-10-14', 'クエスチョニング', '35.6803997', '132.2563233'),
(20, 'Asako', '$2y$10$IttVU6SvkTrDdHcwIbnCU.4ybn7WuvNFzKsdYsbGy6Q5qo.GTdJpW', 'http://meets.email/uploadfiles/userphoto/1602649140972.png', '2', '1', '', '2020-10-13 21:19:00', '', 'Tokyo', '2000-11-16', 'ボーイッシュ', '35.6803997', '132.2563233'),
(21, 'Hanako', '$2y$10$4FLj5pY7heUdSBlzUkQEbejwnxjeIU2npm4FUm5XIuIO.SXe2mp2q', 'http://meets.email/uploadfiles/userphoto/1602658889610.png', '2', '1', '', '2020-10-14 00:01:29', '', 'Tokyo', '1998-11-15', 'Xジェンダー', '35.6803997', '132.2563233'),
(22, 'Yukiko', '$2y$10$yOhrkY3NfkWQ0ojA5bB3JOux7mLDujLfOS/1l239Mpic.pCHkQDe6', 'http://meets.email/uploadfiles/userphoto/1602661500797.png', '1', '1', '', '2020-10-14 00:45:00', '', 'Tokyo', '1997-10-14', 'Xジェンダー', '35.6803997', '139.7690174'),
(23, 'Saori', '$2y$10$7545Dsr69flJtiNHh5q2OObgW32FFVHWIMt79BEvka1PuBgkxYsFC', 'http://meets.email/uploadfiles/userphoto/1603060478746.png', '2', '1', '', '2020-10-18 15:34:38', '', 'Tokyo', '2000-10-18', 'フェム', '35.6803997', '139.7690174'),
(24, 'Misuako', '$2y$10$jOpZucrpxD2Gk36VpLSaHe57KwIkQjD/2bJMTR75jbL9dTjTV.Fai', 'http://meets.email/uploadfiles/userphoto/1603060949388.png', '2', '1', '', '2020-10-18 15:42:29', '', 'Tokyo', '2001-10-18', 'フェム', '35.6803997', '139.7690174'),
(25, 'Hitomi', '$2y$10$GRp3T.KV0y4OsTY6/xbq8.wXiSpX5V/gMuqbTbUJGVzUWTgHP4FRm', 'http://meets.email/uploadfiles/userphoto/1603064349592.png', '1', '1', '', '2020-10-18 16:39:09', '', 'Tokyo', '2000-10-18', 'クエスチョニング', '35.6803997', '139.7690174'),
(26, 'Yosiko', '$2y$10$oQDNKWdZiaULVunVXOgBkOf88t6VHz.QISsIAQtB7veyNFp.Voj.6', 'http://meets.email/uploadfiles/userphoto/1603067832834.png', '1', '1', '', '2020-10-18 17:37:12', 'I am chaeyeon', 'Tokyo', '2000-11-15', '中性', '35.6803997', '132.2563233'),
(29, 'Shungko', '$2y$10$WeCxh36QONOvgkbS3HsY1e33Gr2MMEzdes/9rw0s2ESUcQu7rIIz2', 'http://meets.email/uploadfiles/userphoto/1603150456422.png', '2', '1', '', '2020-10-19 16:34:16', '', 'Fukuoka', '1996-10-19', 'フェム', '33.5901838', '130.4016888'),
(30, 'Star', '$2y$10$jGY.glSZtNu6Q6Sn1ktkfO42dJU/l.sqgSJLxQLDVny7/F025gUNi', 'http://meets.email/uploadfiles/userphoto/1603789191430.png', '1', '1', '', '2020-10-27 01:59:51', 'I am going to find my friend here.', 'Fukuoka', '1998-10-27', 'ボーイッシュ', '33.5901838', '130.4016888'),
(31, 'Kyosiko', '$2y$10$Sw99rATPKu7IfsOHqiSaBefrpAoJ0lWw2GWYwFhffALkuY9J/1qui', 'http://meets.email/uploadfiles/userphoto/1603790213732.png', '1', '1', '', '2020-10-27 02:16:53', 'I am going to find friend here.', 'Tokyo', '2020-10-25', 'Xジェンダー', '', ''),
(32, 'Haruko', '$2y$10$1uPMttONi2yHM4NVuQAVP.GPHcLKHbRLD4W/56M9yTXDVFnHYO3Aa', 'http://meets.email/uploadfiles/userphoto/1603791153546.png', '1', '1', '', '2020-10-27 02:32:33', '', 'Tokyo', '2000-10-27', 'FTM', '35.6803997', '139.7690174'),
(33, 'Yuri', '$2y$10$C1PqztTK5Ix.zK9Csj12vOR6.HXzgSCN3w6PANZZE9VX2fQewVbsC', 'http://meets.email/uploadfiles/userphoto/1603792062241.png', '0', '1', '', '2020-10-27 02:41:21', 'I am going to find my friend here.', 'Tokyo', '2020-09-25', 'MTF', '', ''),
(34, 'Aimi', '$2y$10$TISuJRHIM94KTX6XYDwU0erer6sz9SIR5cwpjN.NWflj5.twdvopm', 'http://meets.email/uploadfiles/userphoto/1603856041191.png', '0', '1', '', '2020-10-27 20:34:01', 'I am going to find love here.	', 'Tokyo', '1998-10-28', 'MTF', '35.6803997', '139.7690174'),
(35, 'Aimiko', '$2y$10$E6J4moVPv3bGaI/o8xYwROB8mqGY1jxqngNkH4ndQX5hD2udD29y2', 'http://meets.email/uploadfiles/userphoto/1603858575990.png', '1', '1', '', '2020-10-27 21:16:15', 'I am going to find friend here.', 'Tokyo', '2000-10-28', 'MTF', '35.6803997', '139.7690174'),
(36, 'Misaiko', '$2y$10$unE80m5FXj2.EIPGKtQpM.bo/kill0jjtq8sT2OHhXQQEPO8pcQ/S', 'http://meets.email/uploadfiles/userphoto/1603861620201.png', '0', '1', '', '2020-10-27 22:07:00', 'I am going to find friend here.', 'Osaka', '1998-10-28', 'FTM', '34.6937249', '135.5022535');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user1`
--

CREATE TABLE `tb_user1` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `joined_date` varchar(255) NOT NULL,
  `is_plus_member` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user1`
--

INSERT INTO `tb_user1` (`id`, `user_id`, `token`, `joined_date`, `is_plus_member`) VALUES
(3, '5f22d9cdd2c7032c76cf9457', 'd2wt5_Xp90dZokPOLlLtfk:APA91bGlLBGjVtwrRbGYt1MqLuhy8WT4xFKy1jHvXIKXljgyyvJhud_gy3nGaaXOEQ7hgUYzl5RxMjZWAc3xalH5fq5X0d4tC-GoFWJ8WGwsmsaCz65AN6WTyOkIA2Tu5p2pZcZDDjSH', '1605067967000', 'false'),
(4, '5f2ad841b573b835e52201c7', 'd2wt5_Xp90dZokPOLlLtfk:APA91bGlLBGjVtwrRbGYt1MqLuhy8WT4xFKy1jHvXIKXljgyyvJhud_gy3nGaaXOEQ7hgUYzl5RxMjZWAc3xalH5fq5X0d4tC-GoFWJ8WGwsmsaCz65AN6WTyOkIA2Tu5p2pZcZDDjSH', '1605067924000', 'false');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_attr`
--
ALTER TABLE `tb_attr`
  ADD PRIMARY KEY (`attr_id`);

--
-- Indexes for table `tb_block`
--
ALTER TABLE `tb_block`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_comment`
--
ALTER TABLE `tb_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_follow`
--
ALTER TABLE `tb_follow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_like`
--
ALTER TABLE `tb_like`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_notification`
--
ALTER TABLE `tb_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_post`
--
ALTER TABLE `tb_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_purpose`
--
ALTER TABLE `tb_purpose`
  ADD PRIMARY KEY (`purpose_id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user1`
--
ALTER TABLE `tb_user1`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_attr`
--
ALTER TABLE `tb_attr`
  MODIFY `attr_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `tb_block`
--
ALTER TABLE `tb_block`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `tb_comment`
--
ALTER TABLE `tb_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_follow`
--
ALTER TABLE `tb_follow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;

--
-- AUTO_INCREMENT for table `tb_like`
--
ALTER TABLE `tb_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_notification`
--
ALTER TABLE `tb_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `tb_post`
--
ALTER TABLE `tb_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tb_purpose`
--
ALTER TABLE `tb_purpose`
  MODIFY `purpose_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `tb_user1`
--
ALTER TABLE `tb_user1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
