-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 23, 2020 at 02:24 AM
-- Server version: 5.6.49-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meets_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_video_verify`
--

CREATE TABLE `tb_video_verify` (
  `id` int(100) NOT NULL,
  `sender_id` varchar(255) CHARACTER SET utf8 NOT NULL,
  `receiver_id` varchar(255) CHARACTER SET utf8 NOT NULL,
  `sender_status` int(100) NOT NULL,
  `receiver_status` int(100) NOT NULL,
  `verify_room_id` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_video_verify`
--

INSERT INTO `tb_video_verify` (`id`, `sender_id`, `receiver_id`, `sender_status`, `receiver_status`, `verify_room_id`) VALUES
(9, '5f22d9cdd2c7032c76cf9457', '5f2ad841b573b835e52201c7', -1, -1, '5f2ad841b573b835e52201c7_5f22d9cdd2c7032c76cf9457');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_video_verify`
--
ALTER TABLE `tb_video_verify`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_video_verify`
--
ALTER TABLE `tb_video_verify`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
